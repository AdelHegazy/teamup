/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gazar;

import iti36.gp.teamup.Constant.SkillsStatusConstant;
import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.SkillsStatus;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import iti36.gp.teamup.util.DbOpenSession;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author AhmedElGazzar
 */
public class daoImpl extends CommonOperationImp implements dao {

    Session session;
    private static final String HQL_LOGIN = "FROM Users u WHERE u.email =? AND u.password =? ";

    private static final String SELECT_BY_ID = "FROM Team t where t.id = ?";

    private static final String UserHasTeam_BY_USERID_TEAMID = "DELETE * FROM UserHasTeam u where u.user_id =? and u.team_id=?";

    @Override
    public Team getTeamById(int id) {

//        Team team = new Team();
//
//        team = (Team)getData(SELECT_BY_ID);
        Team team = get(Team.class, id);
//        Team team = null;
//        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
//        session.getTransaction().begin();
//        team = (Team) session.get(Team.class, id);
//
//        session.getTransaction().commit();

        System.out.println("team by id" + team.getShortDescription());
        return team;
    }



    @Override
    public boolean deleteUserFromTeam(int userId, int teamId) {

        Team team = get(Team.class, teamId);
//        team.getUserHasTeams();
//        teamId = team.getId();
//        UserHasTeam uht = get(UserHasTeam.class, teamId);
        Users user = get(Users.class, userId);
        session.delete(user);
        System.out.println("user id : " + userId + "team id :" + teamId);
        team.getUserHasTeams().remove(user);

//        Users users = uht.getUsers();
//        
//        Query query = 
//        Object data = getData(UserHasTeam_BY_USERID_TEAMID);
//        remove(user);
        System.out.println("user deleted");
        return true;
    }

    public void removeUserFromClass(int teamId, int userId) {
        Transaction transaction = null;
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        try {
            transaction = session.beginTransaction();
            Team tClass = (Team) session.get(Team.class, teamId);
            Users tStudent = (Users) session.get(Users.class, userId);
            tClass.getUserHasTeams().remove(tStudent);
            session.saveOrUpdate(tClass);
            transaction.commit();
            System.out.println("removed");
        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

//    public static void main(String args[]) {
//
//        daoImpl d = new daoImpl();
////        d.getTeamById(1);
////        d.addSkill(skill);
////Team t=new Team();
////        d.getTeamById(1);
////        Users u = new Users();
//        //login(u.getName(),u.getPassword());
////        Team t = new Team();
////        Users u1 = new Users();
////        u1.setId(2);
////        Team
////        d.deleteUserFromTeam(u1, 1);
//        d.login("ahmed", "123");
////        
////        SkillCategory c = new SkillCategory();
////        c.setName("Sport");
////        //skill.setSkillCategory(c);
////        SkillsStatus ss = new SkillsStatus(SkillsStatusConstant.BEGINNER);
//
////        Skills s = new Skills();
////
////        d.addSkill(1,"handballlll");
//        d.removeUserFromClass(1, 2);
//        //d.deleteUserFromTeam(1, 1);
//    }

}
