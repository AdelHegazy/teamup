/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.util;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author adel
 */
public class ImageUtil {

    private final static String JPG = "jpg";
    public static final int WIDTH = 268;
    public static final int HIGHT = 249;
    private final static String IMAGE_BASE = "data:image/jpg;base64,";

    public static String resizeImage(byte[] img, int width, int height) {
        String imageUri = "";
        try {
            if (img != null) {
                ByteArrayInputStream in = new ByteArrayInputStream(img);
                BufferedImage Bufimg = ImageIO.read(in);
                Image scaledImage = Bufimg.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0, 0, 0), null);
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                ImageIO.write(imageBuff, JPG, buffer);
                BASE64Encoder encoder = new BASE64Encoder();
                imageUri = encoder.encode(buffer.toByteArray());
            } else {
                return null;
            }

        } catch (IOException ex) {
            Logger.getLogger(ImageUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return imageUri;
    }

    public static byte[] getImage(String imageString) {
        byte[] imageByte = null;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);

        } catch (IOException ex) {
            Logger.getLogger(ImageUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return imageByte;
    }

    public static String encodeImage(byte[] img, int width, int height) {
        String imageUri = IMAGE_BASE;
        try {
            imageUri = imageUri + resizeImage(img, width, height);
        } catch (Exception ex) {
            Logger.getLogger(ImageUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return imageUri;
    }

}
