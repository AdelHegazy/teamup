/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.util;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author adel
 */
public class DbOpenSession {

    private SessionFactory sessionfactory;
    private static DbOpenSession instance;

    public static synchronized DbOpenSession getInstance() {
        if (instance == null) {
            instance = new DbOpenSession();
        }
        return instance;
    }

    private DbOpenSession() {
        openconnection();
    }

    private void openconnection() {
        try {
            sessionfactory = new Configuration().configure().buildSessionFactory();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void closeConnection() {
        try {
            sessionfactory.close();
        } catch (Exception ex) {
            Logger.getLogger(DbOpenSession.class.getName()).log(Level.SEVERE, null, ex);

        }
    }
    public SessionFactory getSessionfactory() {
        return sessionfactory;
    }

    public void setSessionfactory(SessionFactory sessionfactory) {
        this.sessionfactory = sessionfactory;
    }
}
