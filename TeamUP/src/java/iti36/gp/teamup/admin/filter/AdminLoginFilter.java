/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.admin.filter;

import iti36.gp.teamup.admin.AdminBean;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author AhmedElGazzar
 */
public class AdminLoginFilter implements Filter{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            HttpServletRequest reqt = (HttpServletRequest) request;
            HttpServletResponse resp = (HttpServletResponse) response;
            
            AdminBean adminBean = (AdminBean) reqt.getSession().getAttribute("adminBean");
            String reqURI = reqt.getRequestURI();
            System.out.println("iti36.gp.teamup.admin.filter.doFilter()" + reqURI);
            
            if(adminBean == null || !adminBean.isLogged){
                if (reqURI.endsWith("/AdminPages/") || reqURI.indexOf("/AdminPages/pages/examples/login.xhtml") >= 0) {
                    chain.doFilter(request, response);
                } else {
                    resp.sendRedirect(reqt.getServletContext().getContextPath() + "/AdminPages/pages/examples/login.xhtml");
                }
            } else {
                chain.doFilter(request, response);
            }
            
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void destroy() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
