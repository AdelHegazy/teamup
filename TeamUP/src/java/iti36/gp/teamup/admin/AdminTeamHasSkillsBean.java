/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.admin;

import iti36.gp.teamup.dao.imp.AdminDao;
import iti36.gp.teamup.entity.TeamHasSkill;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author AhmedElGazzar
 */
@ManagedBean
@SessionScoped
public class AdminTeamHasSkillsBean implements java.io.Serializable{

    /**
     * Creates a new instance of AdminTeamHasSkills
     */
    public AdminTeamHasSkillsBean() {
    }
    
    public List<TeamHasSkill> getTeamMembersByUser(int teamId){
        AdminDao dao = new AdminDao();
        List<TeamHasSkill> teamReport = dao.getTeamSkills(teamId);
        
        return teamReport;
    }
}
