/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.admin;

import iti36.gp.teamup.dao.imp.AdminDao;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.Users;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author AhmedElGazzar
 */
@ManagedBean
@SessionScoped
public class AdminBean implements java.io.Serializable {

    /**
     * Creates a new instance of AdminBean
     */
    public static boolean isLogged = false;
    
    @ManagedProperty(value = "#{email}")
    private String email;
    @ManagedProperty(value = "#{password}")
    private String password;

    public AdminBean() {
    }

    public String adminLogin() {
        AdminDao dao = new AdminDao();
        Users login = dao.adminLogin(email, password);
        if (login == null) {
            System.out.println("not logging admin");
            return "login.xhtml";
        } else {
            System.out.println("logging admin");
            isLogged = true;
            return "activationskills.xhtml";
        }
    }
    
    public void logout() {
        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.invalidateSession();
            System.out.println("logged out");
            externalContext.redirect(externalContext.getRequestContextPath() + "/AdminPages/pages/examples/login.xhtml");
            isLogged = false;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
