/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.admin;

import iti36.gp.teamup.dao.imp.AdminDao;
import iti36.gp.teamup.entity.SkillCategory;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author AhmedElGazzar
 */
@ManagedBean
@RequestScoped
public class AdminSkillCategoryBean implements java.io.Serializable {

    @ManagedProperty(value = "#{id}")
    private Integer id;

    @ManagedProperty(value = "#{name}")
    private String name;

    List<SkillCategory> names = new ArrayList<>();
    AdminDao admin = new AdminDao();

    public List<SkillCategory> getAllSkillCategory() {

//        List<String> catList = new ArrayList<>();
        List<SkillCategory> allSkillCategory = admin.getAllSkillCategory();
//        for (SkillCategory skillCategory : allSkillCategory) {
//            catList.add(skillCategory.getName());
//        }
        return allSkillCategory;
//        return (List<SkillCategory>)(SkillCategory)catList;
    }

    public void addCategory() {
        admin.addCategory(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SkillCategory> getNames() {
        return names;
    }

    public void setNames(List<SkillCategory> names) {
        this.names = names;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
