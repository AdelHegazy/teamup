/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.admin;

import iti36.gp.teamup.dao.imp.AdminDao;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.TeamMemberType;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

/**
 *
 * @author AhmedElGazzar
 */
@ManagedBean
@ViewScoped
public class AdminUserHasTeamBean implements java.io.Serializable{

    /**
     * Creates a new instance of AdminUserHasTeamBean
     */
    private Integer id;
    private Team team = new Team();
    private Users users;
    private TeamMemberType teamMemberType;
    

    public AdminUserHasTeamBean() {
    }

    public List<UserHasTeam> getTeamReport(){
        AdminDao dao = new AdminDao();
        List<UserHasTeam> teamReport = dao.getTeamReport();
        
        return teamReport;
    }
    
    
    public List<UserHasTeam> getTeamMembersByUser(int teamId){
        AdminDao dao = new AdminDao();
        List<UserHasTeam> teamReport = dao.getTeamMembersByUser(teamId);
        
        return teamReport;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public TeamMemberType getTeamMemberType() {
        return teamMemberType;
    }

    public void setTeamMemberType(TeamMemberType teamMemberType) {
        this.teamMemberType = teamMemberType;
    }

    
}
