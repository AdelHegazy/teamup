/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.admin;

import iti36.gp.teamup.dao.imp.AdminDao;
import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.SkillsStatus;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author AhmedElGazzar
 */
@ManagedBean
@SessionScoped
public class AdminSkillsBean implements java.io.Serializable{

    /**
     * Creates a new instance of AdminSkillsBean
     */
    @ManagedProperty(value = "#{id}")
    private Integer id;
    @ManagedProperty(value = "#{name}")
    private String name;
    
    private SkillsStatus skillsStatus;
    private Integer catId;
    private Skills skill;
    
    @ManagedProperty(value = "#{skillCategory}")
    private SkillCategory skillCategory = new SkillCategory();
    
    private int statId;
    List<Skills> s = new ArrayList<>();
    
    AdminDao admin = new AdminDao();

    public AdminSkillsBean() {
    }

    private boolean enabled;

    public boolean activeToggle(int skillsId,int statusId){
         System.out.println("in active toggle"+enabled);
        if(statusId==3){
            acvtiveSkills(skillsId);
            return enabled=false;
        }else if(statusId==1) {
            deacvtiveSkills(skillsId);
        }
        return enabled=false;
    }
    
    public void toggle(int skillsId,int statusId) {
        System.out.println("in toggle"+enabled);
//        int id1 = skill.getSkillsStatus().getId();
        if(statusId==0 || statusId==2 || statusId==3){
            acvtiveSkills(skillsId);
        }else if(statusId==1) {
            deacvtiveSkills(skillsId);
        }
    }

    public boolean inputSwitch(int statId){
        if(statId == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public int getStatId() {
        return statId;
    }

    public void setStatId(int statId) {
        this.statId = statId;
    }
    
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public SkillsStatus getSkillsStatus() {
        return skillsStatus;
    }

    public void setSkillsStatus(SkillsStatus skillsStatus) {
        this.skillsStatus = skillsStatus;
    }

   
    public List<Skills> getWaitingSkills() {
        s = admin.getWaitingSkills();
        return s;
    }
    
    public List<Skills> getCountOfWaitingSkills(){
        return admin.getCountOfWaitingSkills();
    }

    public List<Skills> getAllSkills() {
        return admin.getAllSkills();
    }
    
    public void acvtiveSkills(int skillsId){
        System.out.println("acvtiveSkills");
        admin.acvtiveSkills(skillsId);
//        return "/AdminPages/pages/examples/manageskills.xhtml";
    }
    
    public void deacvtiveSkills(int skillsId){
        System.out.println("deacvtiveSkills");
        admin.deacvtiveSkills(skillsId);
//        return "/AdminPages/pages/examples/manageskills.xhtml";
    }    

    public void addSkill() {
//        List<Skills> list = new ArrayList<>();
//        list = admin.getAllSkills();
//        if(list.contains(name)){
//            System.out.println("if exist");
//        }
//        else{
            admin.addSkill(catId, name);
//        }
    }

    public void setStatusName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Skills getSkill() {
        return skill;
    }

    public void setSkill(Skills skill) {
        this.skill = skill;
    }

    public SkillCategory getSkillCategory() {
        return skillCategory;
    }

    public void setSkillCategory(SkillCategory skillCategory) {
        this.skillCategory = skillCategory;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

}
