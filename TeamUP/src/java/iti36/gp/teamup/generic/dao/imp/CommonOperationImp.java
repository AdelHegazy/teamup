/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.generic.dao.imp;

import iti36.gp.teamup.generic.dao.CommonOperationInt;
import iti36.gp.teamup.util.DbOpenSession;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
/**
 *
 * @author adel
 */
import org.hibernate.Query;

public class CommonOperationImp implements CommonOperationInt {

    @Override
    public <T> T add(T type) {
        try {
            Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
            session.beginTransaction();
            session.save(type);
            session.getTransaction().commit();
            session.refresh(type);
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return type;
    }

    @Override
    public <T> void remove(T type) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.beginTransaction();
        session.delete(type);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public <T> T update(T type) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.beginTransaction();
//        session.saveOrUpdate(type);
        session.merge(type);
        session.getTransaction().commit();
        session.refresh(type);
        return type;
    }

    @Override
    public <T> T getData(String query) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
       // session.beginTransaction();
        Query query1 = session.createQuery(query);
        return (T) query1.list();
    }

    @Override
    public <T> T get(final Class<T> type, final int id) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        T object = (T) session.get(type, id);
        session.close();
        return object;
    }

    @Override
    public <T> List<T> getAll(final Class<T> type) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        Criteria crit = session.createCriteria(type);
        return crit.list();
    }


}
