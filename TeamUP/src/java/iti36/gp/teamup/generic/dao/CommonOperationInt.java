/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.generic.dao;

import java.util.List;

/**
 *
 * @author adel
 */
public interface CommonOperationInt {

    public <T> T add(T type);

    public <T> void remove(T type);

    public <T> T update(T type);

    public <T> T getData(String query);

    public <T> T get(final Class<T> type, final int id);

    public <T> List<T> getAll(final Class<T> type);
}
