/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.ws.constatnt;

/**
 *
 * @author adel
 */
public interface UserConstant {

    public static String EMAIL = "email";
    public static String PASSWORD = "password";
    public static String NAME = "name";
    public static String PHONE = "phone";
    public static String LINKED_IN = "linkedIn";
    public static String BIO = "bio";
    public static String ID = "id";
    public static String IMAGE = "image";

}
