/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.ws.imp;

import iti36.gp.teamup.Constant.SkillsStatusConstant;
import iti36.gp.teamup.constant.TeamMemberTypeConstant;
import iti36.gp.teamup.dto.AcceptInvitation;
import iti36.gp.teamup.dto.InviteUser;
import iti36.gp.teamup.dto.SkillCategoryDto;
import iti36.gp.teamup.dto.SkillDto;
import iti36.gp.teamup.dto.UserDto;
import iti36.gp.teamup.entity.ErrorMessage;
import iti36.gp.teamup.entity.MessageHasUsers;
import iti36.gp.teamup.entity.MessageSendFrom;
import iti36.gp.teamup.entity.MessageSendToMe;
import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.gcm.GCMNotification;
import iti36.gp.teamup.gcm.GcmConstatnt;
import iti36.gp.teamup.service.dao.imp.RegistrationService;
import iti36.gp.teamup.util.ImageUtil;
import iti36.gp.teamup.ws.TeamUpWebServices;
import iti36.gp.teamup.ws.constatnt.CreateTeamConstant;
import iti36.gp.teamup.ws.constatnt.TeamConstant;
import iti36.gp.teamup.ws.constatnt.UserConstant;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author adel
 */
@Path("/WebService")
public class TeamUpWebServicesImp implements TeamUpWebServices {

    RegistrationService registrationService = new RegistrationService();
    ObjectMapper mapper = new ObjectMapper();
    private final Map< Map<String, Integer>, Map<String, Integer>> data = new HashMap<>();
    private String category;
    private String skill;
    private Map<String, Integer> categories;
    private Map<String, Integer> skills;

    //http://localhost:8080/TeamUP/WebService/login
    @POST
    @Path("/login")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String loginWs(JSONObject jsonObject) {
        try {
            MessageSendToMe to;
            MessageSendFrom from;
            String json;
            Users user = new Users();
            String email = jsonObject.getString("email");
            String password = jsonObject.getString("password");
            user.setEmail(email);
            user.setPassword(password);
            user = registrationService.login(user);
            if (user != null) {
                user.setProfilePictureBase64(ImageUtil.resizeImage(user.getProfilePicture(), ImageUtil.WIDTH, ImageUtil.HIGHT));
                for (Object object : user.getMessageHasUsersesForTo()) {
                    MessageHasUsers message = (MessageHasUsers) object;
                    to = new MessageSendToMe();
                    to.setMessageHasUserId(message.getId());
                    to.setMessageId(message.getMessage().getId());
                    to.setMessage(message.getMessage().getMessageContent());
                    to.setSenderId(message.getUsersByFrom().getId());
                    user.getSendToMe().add(to);
                }

                for (Object object : user.getMessageHasUsersesForFrom()) {
                    MessageHasUsers message = (MessageHasUsers) object;
                    from = new MessageSendFrom();
                    from.setMessageHasUserId(message.getId());
                    from.setMessageId(message.getMessage().getId());
                    from.setMessage(message.getMessage().getMessageContent());
                    from.setUserId(message.getUsersByTo().getId());
                    user.getSendTo().add(from);
                }
                user.setMessageHasUsersesForFrom(null);
                user.setMessageHasUsersesForTo(null);
                json = mapper.writeValueAsString(user);
            } else {
                json = mapper.writeValueAsString(new ErrorMessage(ErrorMessage.ERROR));
            }
            return json;

        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
//http://localhost:8080/TeamUP/registration

    @POST
    @Path("/registration")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String registrationWs(JSONObject jsonObject) {
        try {

            Users user = new Users();
            user.setName(jsonObject.getString(UserConstant.NAME));
            user.setPassword(jsonObject.getString(UserConstant.PASSWORD));
            user.setPhone(jsonObject.getString(UserConstant.PHONE));
            user.setProfilePicture(ImageUtil.getImage(jsonObject.getString(UserConstant.IMAGE)));
            user.setBio(jsonObject.getString(UserConstant.BIO));
            user.setEmail(jsonObject.getString(UserConstant.EMAIL));
            user.setUrl(jsonObject.getString(UserConstant.LINKED_IN));
            user = registrationService.registration(user);
            user.setProfilePictureBase64(ImageUtil.resizeImage(user.getProfilePicture(), ImageUtil.WIDTH, ImageUtil.HIGHT));
            return mapper.writeValueAsString(user);
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @POST
    @Path("/editProfile")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String editProfileWs(JSONObject jsonObject) {

        try {
            int id = Integer.parseInt(jsonObject.getString(UserConstant.ID));
            Users user = registrationService.getUser(id);
            user.setName(jsonObject.getString(UserConstant.NAME));
            user.setPassword(jsonObject.getString(UserConstant.PASSWORD));
            user.setPhone(jsonObject.getString(UserConstant.PHONE));
            user.setBio(jsonObject.getString(UserConstant.BIO));
            user.setEmail(jsonObject.getString(UserConstant.EMAIL));
            user.setUrl(jsonObject.getString(UserConstant.LINKED_IN));
            user.setProfilePicture(ImageUtil.getImage(jsonObject.getString(UserConstant.IMAGE)));
            user = registrationService.updateProfile(user);
            user.setProfilePictureBase64(ImageUtil.resizeImage(user.getProfilePicture(), ImageUtil.WIDTH, ImageUtil.HIGHT));
            return mapper.writeValueAsString(user);
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @POST
    @Path("/creatTeam")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String createTeam(JSONObject jsonObject) {
        Team team = new Team();
        try {
            int ownerId = Integer.parseInt(jsonObject.getString(CreateTeamConstant.OWNER_ID));
            int skillId = Integer.parseInt(jsonObject.getString(CreateTeamConstant.SKILL_ID));
            team.setTitle(jsonObject.getString(CreateTeamConstant.TITLE));
            team.setDescription(CreateTeamConstant.DESCRIPTION);
            String image = jsonObject.getString(CreateTeamConstant.IMAGE);
            team.setImage(ImageUtil.getImage(image));
            team.setShortDescription(jsonObject.getString(CreateTeamConstant.BIO));
            team.setDurationDays(Integer.parseInt(jsonObject.getString(CreateTeamConstant.DURATION)));
            team = registrationService.createTeam(team, ownerId, skillId);
            return mapper.writeValueAsString(team);
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    @GET
    @Path("/getAllCategory")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String getAllCategory() {
        try {
            List<SkillCategory> category = registrationService.getAllCategory();
            return mapper.writeValueAsString(getCategory(category));
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private List<SkillCategoryDto> getCategory(List<SkillCategory> category) {
        List<SkillCategoryDto> categoryListDto = new ArrayList<>();
        SkillCategoryDto categoryDto;
        List<SkillDto> skillList;
        for (SkillCategory skillCategory : category) {
            categoryDto = new SkillCategoryDto();
            skillList = new ArrayList<>();
            for (Object skillObject : skillCategory.getSkillses()) {
                Skills selectedSkill = (Skills) skillObject;
                SkillDto skillDto = new SkillDto();
                if (selectedSkill.getSkillsStatus().getStatusName().equals(SkillsStatusConstant.ACTIVATED)) {
                    skillDto.setName(selectedSkill.getName());
                    skillDto.setId(selectedSkill.getId());
                    skillDto.setSkillsStatus(selectedSkill.getSkillsStatus().getStatusName());
                    skillDto.setSkillsStatusId(selectedSkill.getSkillsStatus().getId());
                    skillList.add(skillDto);
                }
            }
            categoryDto.setId(skillCategory.getId());
            categoryDto.setName(skillCategory.getName());
            categoryDto.setSkill(skillList);
            categoryListDto.add(categoryDto);
        }
        return categoryListDto;
    }

    @POST
    @Path("/getUserForNewTeam")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String getSuggestionUsersForNewTeam(JSONObject jsonObject) {
        List<UserDto> users;
        try {
            int ownerId = Integer.parseInt(jsonObject.getString(CreateTeamConstant.OWNER_ID));
            int skillId = Integer.parseInt(jsonObject.getString(CreateTeamConstant.SKILL_ID));
            List<Users> userList = registrationService.getSuggestionUsersForNewTeam(skillId, ownerId);
            users = new ArrayList<>();
            UserDto userDto;
            for (Users user : userList) {
                userDto = new UserDto();
                userDto.setEmail(user.getEmail());
                userDto.setId(user.getId());
                userDto.setPhone(user.getPhone());
                userDto.setProfilePictureBase64(ImageUtil.resizeImage(user.getProfilePicture(), ImageUtil.WIDTH, ImageUtil.HIGHT));
                userDto.setBio(user.getBio());
                userDto.setUrl(user.getUrl());
                userDto.setName(user.getName());
                users.add(userDto);
            }
            return mapper.writeValueAsString(users);
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    @POST
    @Path("/inviteUsersToTeam")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String inviteUsersToTeam(JSONObject jsonObject) {
        try {
            List<Users> usersId = new ArrayList<>();
            int teamId = Integer.parseInt(jsonObject.getString("teamId"));
            JSONArray users = jsonObject.getJSONArray("usersId");
            GCMNotification gcmNotification = new GCMNotification();
            for (int i = 0; i < users.length(); i++) {
                JSONObject object = users.getJSONObject(i);
                Users user = registrationService.getUser(Integer.parseInt(object.getString("id")));

                if (user != null) {
                    if (user.getToken() != null) {
                        gcmNotification.registration(user.getToken());
                    }
                    usersId.add(user);
                }
            }
            if (usersId.size() > 0) {
                registrationService.inviteMembersToTeam(teamId, usersId);
                InviteUser invite = new InviteUser("invite", teamId);
                gcmNotification.sendNotification(mapper.writeValueAsString(invite));
                gcmNotification.clear();
            }
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @POST
    @Path("/registrationGcm")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String registrationGcm(JSONObject jsonObject) {
        String json = "";
        try {
            int ownerId = Integer.parseInt(jsonObject.getString(GcmConstatnt.USER_ID));
            String token = jsonObject.getString(GcmConstatnt.TOKEN);
            Users user = registrationService.getUser(ownerId);
            if (user != null) {
                user.setToken(token);
                user = registrationService.updateProfile(user);
                json = mapper.writeValueAsString(user);
            } else {
                json = mapper.writeValueAsString(new ErrorMessage(ErrorMessage.INVALID_ID));
            }
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return json;
    }

    @POST
    @Path("/getTeamById")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String getTeamById(JSONObject jsonObject) {
        try {
            int teamId = Integer.parseInt(jsonObject.getString("id"));
            return mapper.writeValueAsString(registrationService.getTeam(teamId));
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @POST
    @Path("/getMyTeamAsOwner")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String getMyTeamAsOwner(JSONObject jsonObject) {
        try {
            int userId = Integer.parseInt(jsonObject.getString(UserConstant.ID));
            return mapper.writeValueAsString(registrationService.getMyTeamAsOwner(userId));
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @POST
    @Path("/getMyTeamAsMember")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String getMyTeamAsMember(JSONObject jsonObject) {
        try {
            int userId = Integer.parseInt(jsonObject.getString(UserConstant.ID));
            return mapper.writeValueAsString(registrationService.getMyTeamAsMember(userId));
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @POST
    @Path("/changeTeamStatus")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String changeTeamStatus(JSONObject jsonObject) {
        try {
            int teamId = Integer.parseInt(jsonObject.getString(TeamConstant.ID));
            Team team = registrationService.getTeam(teamId);
            boolean cancel = jsonObject.getBoolean(TeamConstant.STATUS);
            GCMNotification gcmNotification = new GCMNotification();
            for (Object object : team.getUserHasTeams()) {
                UserHasTeam userHasTeam = (UserHasTeam) object;
                if (userHasTeam.getUsers().getToken() != null) {
                    gcmNotification.registration(userHasTeam.getUsers().getToken());
                }
            }
            if (cancel) {
                gcmNotification.sendNotification("team" + team.getTitle() + "Has been canceled");
            } else {
                gcmNotification.sendNotification("team" + team.getTitle() + "Has been Activated");
            }
            return mapper.writeValueAsString(registrationService.changeTeamStatus(team, cancel));
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @POST
    @Path("/operationOnInvitation")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String operationOnInvitation(JSONObject jsonObject) {
        try {
            int teamId = Integer.parseInt(jsonObject.getString("teamId"));
            int userId = Integer.parseInt(jsonObject.getString("userId"));
            boolean accept = jsonObject.getBoolean(TeamConstant.ACCEPT);
            Team team = registrationService.getTeam(teamId);
            Users user = registrationService.operationOnInvitation(teamId, userId, accept);
            Users owner = registrationService.getTeamOwner(team);
            GCMNotification gcmNotification = new GCMNotification();
            if (owner != null) {
                if (owner.getToken() != null) {
                    gcmNotification.registration(owner.getToken());
                }
            }
            AcceptInvitation acceptInvitation = new AcceptInvitation();
            if (accept) {
                acceptInvitation.setMessage("accept");
            } else {
                acceptInvitation.setMessage("reject");
            }
            acceptInvitation.setUserName(user.getName());
            acceptInvitation.setTeamId(teamId);
            acceptInvitation.setUserId(userId);
            acceptInvitation.setTeamTitle(team.getTitle());
            gcmNotification.sendNotification(mapper.writeValueAsString(acceptInvitation));
            gcmNotification.clear();
            return mapper.writeValueAsString(user);
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @POST
    @Path("/confirm")
    @Consumes("application/json")
    @Override
    public void confirm(JSONObject jsonObject) {
        try {
            int teamId = Integer.parseInt(jsonObject.getString("teamId"));
            int userId = Integer.parseInt(jsonObject.getString("userId"));
            int ownerId = Integer.parseInt(jsonObject.getString("ownerId"));
            Team team = registrationService.getTeam(teamId);
            Users user = registrationService.getUser(userId);
            Users userOwnerId = registrationService.getUser(ownerId);
            boolean confirm = jsonObject.getBoolean(TeamConstant.CONFIRM);
            int id = registrationService.getTeamHasUserId(user, team, TeamMemberTypeConstant.MEMBER);
            registrationService.confirm(id, confirm);
            GCMNotification gcmNotification = new GCMNotification();
            if (user != null) {
                if (user.getToken() != null) {
                    gcmNotification.registration(user.getToken());
                }
            }
            AcceptInvitation acceptInvitation = new AcceptInvitation();
            if (confirm) {
                acceptInvitation.setMessage("confirm");
            } else {
                acceptInvitation.setMessage("Owner Reject");
            }
            acceptInvitation.setUserName(userOwnerId.getName());
            acceptInvitation.setTeamTitle(team.getTitle());
            gcmNotification.sendNotification(mapper.writeValueAsString(acceptInvitation));
            gcmNotification.clear();
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @POST
    @Path("/addSkill")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String addSkill(JSONObject jsonObject) {
        try {
            List<Integer> skillsId = new ArrayList<>();
            int skillId = 0;
            int userId = Integer.parseInt(jsonObject.getString("userId"));
            JSONArray skillJSONArray = jsonObject.getJSONArray("skill");
            for (int i = 0; i < skillJSONArray.length(); i++) {
                JSONObject object = skillJSONArray.getJSONObject(i);
                skillId = Integer.parseInt(object.getString("id"));
                skillsId.add(skillId);
            }
            Users user = registrationService.addSkill(userId, skillsId, 1);
            return mapper.writeValueAsString(user);
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @POST
    @Path("/addNewSkill")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public String addNewSkill(JSONObject jsonObject) {
        try {
            int userId = Integer.parseInt(jsonObject.getString("userId"));
            int categoryId = Integer.parseInt(jsonObject.getString("categoryId"));
            String skillName = jsonObject.getString("skillName");
            Users user = registrationService.addNewSkill(userId, categoryId, skillName);
            return mapper.writeValueAsString(user);
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    @POST
    @Path("/logOut")
    @Produces("application/json")
    @Consumes("application/json")
    public String logOut(JSONObject jsonObject) {
        try {
            int id = Integer.parseInt(jsonObject.getString("userId"));
            Users user = registrationService.getUser(id);
            user.setToken(null);
            user = registrationService.updateProfile(user);
            if (user.getToken() == null) {
                return "successful";
            }
        } catch (JSONException ex) {
            Logger.getLogger(TeamUpWebServicesImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "failure";
    }
}
