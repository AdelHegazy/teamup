/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.ws;

import iti36.gp.teamup.entity.Users;
import java.util.List;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author adel
 */
public interface TeamUpWebServices {

    public String loginWs(JSONObject jsonObject);

    public String registrationWs(JSONObject jsonObject);

    public String editProfileWs(JSONObject jsonObject);

    public String createTeam(JSONObject jsonObject);

    public String getAllCategory();

    public String getSuggestionUsersForNewTeam(JSONObject jsonObject);

    public String inviteUsersToTeam(JSONObject jsonObject);

    public String registrationGcm(JSONObject jsonObject);

    public String getTeamById(JSONObject jsonObject);
    
    public String getMyTeamAsOwner(JSONObject jsonObject);

    public String getMyTeamAsMember(JSONObject jsonObject);
    
    public String changeTeamStatus(JSONObject jsonObject);
    
    public String operationOnInvitation(JSONObject jsonObject);
    
    public void confirm(JSONObject jsonObject);
    
    public String addSkill(JSONObject jsonObject);
    
    public String addNewSkill(JSONObject jsonObject);
    
    public String logOut(JSONObject jsonObject);

}
