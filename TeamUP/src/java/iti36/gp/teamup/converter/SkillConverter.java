/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.converter;

import iti36.gp.teamup.beans.CategoryBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author adel
 */
@ManagedBean
@RequestScoped
public class SkillConverter implements Converter {
    
    @ManagedProperty("#{categoryBean}")
    private CategoryBean categoryBean;
    
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String inputString) {
//        return String.valueOf(dropdownView.getSkillMap().get(inputString));
        categoryBean.setSkillName(inputString);
        return null;
    }
    
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object modelValue) {
        if (modelValue == null) {
            return "";
        } else {
            return String.valueOf(modelValue);
        }
    }

    public void setCategoryBean(CategoryBean categoryBean) {
        this.categoryBean = categoryBean;
    }
    
   
    
}
