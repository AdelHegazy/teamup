/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.constant;

/**
 *
 * @author adel
 */
public class TeamMemberTypeConstant {

    public static final String OWNER = "owner";
    public static final int OWNER_ID = 1;
    public static final String MEMBER = "member";
    public static final int MEMBER_ID = 2;
}
