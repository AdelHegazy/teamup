/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.constant;

/**
 *
 * @author adel
 */
public class TeamMemberStatusConstant {

    public static final String ACCEPT = "accept";
    public static final int ACCEPT_ID = 1;
    public static final String REJECT = "reject";
    public static final int REJECT_ID = 2;
    public static final String CONFIRM = "confirm";
    public static final int CONFIRM_ID = 3;
    public static final String INVITE = "invite";
    public static final int INVITE_ID = 4;
}
