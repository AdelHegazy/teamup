/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.Constant;

/**
 *
 * @author AhmedElGazzar
 */
public class UserSkillLevelConstant {

    public static final String BEGINNER = "beginner";
    public static final int BEGINNER_ID = 1;
    public static final String INTERMEDIATE = "intermediate";
    public static final int INTERMEDIATE_ID = 2;
    public static final String PROFESSIONAL = "professional";
    public static final int PROFESSIONAL_ID = 3;
}
