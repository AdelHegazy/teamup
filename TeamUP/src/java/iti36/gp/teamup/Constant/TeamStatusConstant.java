/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.constant;

/**
 *
 * @author adel
 */
public class TeamStatusConstant {

    public static final String CANCELLED = "cancelled";
    public static final int CANCELLED_ID = 1;
    public static final String ACTIVE = "active";
    public static final int ACTIVE_ID = 2;
    public static final String EXPIRED = "expired";
    public static final int EXPIRED_ID = 3;

}
