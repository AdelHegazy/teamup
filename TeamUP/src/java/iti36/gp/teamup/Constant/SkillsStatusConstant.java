/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.Constant;

/**
 *
 * @author AhmedElGazzar
 */
public class SkillsStatusConstant {
    public static final String ACTIVATED = "activated";
    public static final String DEACTIVATED = "deactivated";
    public static final String WAITING = "waiting";
    public static final int WAITING_ID = 3;
    public static final int DEACTIVATED_ID = 2;
    public static final int ACTIVATED_ID = 1;
}
