/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dto;

/**
 *
 * @author adel
 */
public class InviteUser {

    private String message;
    private int teamId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public InviteUser(String message, int teamId) {
        this.message = message;
        this.teamId = teamId;
    }
}
