package iti36.gp.teamup.dto;

import java.util.List;

public class SkillCategoryDto  implements java.io.Serializable {
     private Integer id;
     private String name;
     List <SkillDto> skill;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SkillDto> getSkill() {
        return skill;
    }

    public void setSkill(List<SkillDto> skill) {
        this.skill = skill;
    }

 
     
}


