/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dto;

import iti36.gp.teamup.entity.TeamMemberStatus;
import iti36.gp.teamup.entity.Users;

/**
 *
 * @author adel
 */
public class UserStatusDto {
    private Users user;
    private TeamMemberStatus teamMemberStatus;

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public TeamMemberStatus getTeamMemberStatus() {
        return teamMemberStatus;
    }

    public void setTeamMemberStatus(TeamMemberStatus teamMemberStatus) {
        this.teamMemberStatus = teamMemberStatus;
    }
    
    
}
