/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dto;

/**
 *
 * @author adel
 */
public class SkillDto {
     private int id;
     private String skillsStatus;
     private int skillsStatusId;
     private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkillsStatus() {
        return skillsStatus;
    }

    public void setSkillsStatus(String skillsStatus) {
        this.skillsStatus = skillsStatus;
    }

    public int getSkillsStatusId() {
        return skillsStatusId;
    }

    public void setSkillsStatusId(int skillsStatusId) {
        this.skillsStatusId = skillsStatusId;
    }
     
}
