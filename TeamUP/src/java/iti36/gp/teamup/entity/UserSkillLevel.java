package iti36.gp.teamup.entity;
// Generated May 8, 2016 6:16:55 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import org.codehaus.jackson.annotate.JsonManagedReference;

/**
 * UserSkillLevel generated by hbm2java
 */
public class UserSkillLevel  implements java.io.Serializable {


     private int id;
     private String level;
     @JsonManagedReference
     private Set userHasSkills = new HashSet(0);

    public UserSkillLevel() {
    }

	
    public UserSkillLevel(int id, String level) {
        this.id = id;
        this.level = level;
    }
    public UserSkillLevel(int id, String level, Set userHasSkills) {
       this.id = id;
       this.level = level;
       this.userHasSkills = userHasSkills;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getLevel() {
        return this.level;
    }
    
    public void setLevel(String level) {
        this.level = level;
    }
    public Set getUserHasSkills() {
        return this.userHasSkills;
    }
    
    public void setUserHasSkills(Set userHasSkills) {
        this.userHasSkills = userHasSkills;
    }




}


