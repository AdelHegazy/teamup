/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.entity;

/**
 *
 * @author adel
 */
public class MessageSendFrom {
    private int messageId;
    private String message;
    private int messageHasUserId;
    private int userId;

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getMessageHasUserId() {
        return messageHasUserId;
    }

    public void setMessageHasUserId(int messageHasUserId) {
        this.messageHasUserId = messageHasUserId;
    }
    
}
