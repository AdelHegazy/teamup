/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.entity;

/**
 *
 * @author adel
 */
public class ErrorMessage {

    public static String ERROR = "email and password didn't match";
    public static String INVALID_ID = "Invalid ID";
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ErrorMessage(String message) {
        this.error = message;
    }

}
