package iti36.gp.teamup.entity;
// Generated May 8, 2016 6:16:55 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import org.codehaus.jackson.annotate.JsonManagedReference;

/**
 * TeamMemberType generated by hbm2java
 */
public class TeamMemberType  implements java.io.Serializable {


     private int id;
     private String memberType;
       @JsonManagedReference
     private Set userHasTeams = new HashSet(0);

    public TeamMemberType() {
    }

	
    public TeamMemberType(int id, String memberType) {
        this.id = id;
        this.memberType = memberType;
    }
    public TeamMemberType(int id, String memberType, Set userHasTeams) {
       this.id = id;
       this.memberType = memberType;
       this.userHasTeams = userHasTeams;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getMemberType() {
        return this.memberType;
    }
    
    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }
    public Set getUserHasTeams() {
        return this.userHasTeams;
    }
    
    public void setUserHasTeams(Set userHasTeams) {
        this.userHasTeams = userHasTeams;
    }




}


