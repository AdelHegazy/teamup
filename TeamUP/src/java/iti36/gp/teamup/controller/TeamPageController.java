/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author adel
 */
@ManagedBean(name = "team")
@SessionScoped
public class TeamPageController {

    public String renderTeamPage() {

        return "createTeam.xhtml";

    }
}
