/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.beans;

import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.service.dao.imp.RegistrationService;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author adel
 */
@ManagedBean(name = "categoryBean")
@ApplicationScoped
public class CategoryBean {

    private static RegistrationService registrationService;
    private static List<SkillCategory> category;
    private  String selectedCategory;
    private  int skill;
    private  String skillName;

    static {

        registrationService = new RegistrationService();


    }

    public static RegistrationService getRegistrationService() {
        return registrationService;
    }

    public static void setRegistrationService(RegistrationService registrationService) {
        CategoryBean.registrationService = registrationService;
    }

    public static List<SkillCategory> getCategory() {
       category = registrationService.getAllCategory();
        return category;
    }

    public static void setCategory(List<SkillCategory> category) {
        CategoryBean.category = category;
    }

    public String getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(String selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }




   

}
