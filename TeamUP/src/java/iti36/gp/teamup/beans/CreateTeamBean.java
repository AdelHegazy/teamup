package iti36.gp.teamup.beans;

import iti36.gp.teamup.Constant.SkillsStatusConstant;
import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.service.dao.imp.RegistrationService;
import iti36.gp.teamup.util.ImageUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

@ManagedBean
@SessionScoped
public class CreateTeamBean implements Serializable {

    private final Map<String, Map<String, Integer>> data = new HashMap<>();
    private String category;
    private String skill;
    private Map<String, String> categories;
    private Map<String, Integer> skills;
    private String title;
    private String discription;
    private String bio;
    private int duration;
    @ManagedProperty("#{categoryBean}")
    private CategoryBean categoryBean;
    @ManagedProperty("#{fileUploadView}")
    private FileUploadView file;
    private RegistrationService registrationService;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    List<Users> users;
    private Team team;
    private DataModel<Users> userModel;

    @PostConstruct
    public void init() {
        registrationService = new RegistrationService();
        List<SkillCategory> ob = CategoryBean.getCategory();
        categories = new HashMap<String, String>();
        Map<String, Integer> skillMap = new HashMap<>();
        for (SkillCategory skillCategory : ob) {
            categories.put(skillCategory.getName(), skillCategory.getName());
            for (Object skillObject : skillCategory.getSkillses()) {
                Skills selectedSkill = (Skills) skillObject;
                if (selectedSkill.getSkillsStatus().getStatusName().equals(SkillsStatusConstant.ACTIVATED)) {
                    skillMap.put(selectedSkill.getName(), selectedSkill.getId());
                }
            }
            data.put(skillCategory.getName(), skillMap);
            skillMap = new HashMap<>();
        }
    }

    public Map<String, Map<String, Integer>> getData() {
        return data;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public Map<String, String> getCategories() {
        return categories;
    }

    public void setCategories(Map<String, String> categories) {
        this.categories = categories;
    }

    public Map<String, Integer> getSkills() {
        return skills;
    }

    public void setSkills(Map<String, Integer> skills) {
        this.skills = skills;
    }

    public void setCategoryBean(CategoryBean categoryBean) {
        this.categoryBean = categoryBean;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public void setFile(FileUploadView file) {
        this.file = file;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<Users> getUsers() {
        return users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }

    public DataModel<Users> getUserModel() {
        return userModel;
    }

    public void setUserModel(DataModel<Users> userModel) {
        this.userModel = userModel;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public void onCategoryChanged() {
        if (category != null && !category.equals("")) {
            skills = data.get(category);
        } else {
            skills = new HashMap<>();
        }
    }

    public void validateSkill() {
        FacesMessage msg;
        if (categoryBean.getSkill() != 0 && categoryBean.getSelectedCategory() != null) {
            msg = new FacesMessage("Selected", categoryBean.getSkill() + " of " + categoryBean.getSelectedCategory());
        } else {
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid", "Skill is not selected.");
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String create() {
        Team team = new Team();
        team.setTitle(title);
        team.setDescription(discription);
        team.setImage(file.getFile().getContents());
        team.setShortDescription(bio);
        team.setDurationDays(duration);
        this.team = registrationService.createTeam(team, loginBean.getCurrentUser().getId(), categoryBean.getSkill());
        loginBean.setCurrentUser(registrationService.getUser(loginBean.getCurrentUser().getId()));
        users = registrationService.getSuggestionUsersForNewTeam(categoryBean.getSkill(), loginBean.getCurrentUser().getId());
        for (Users user : users) {
            user.setProfilePictureBase64(ImageUtil.encodeImage(user.getProfilePicture(), ImageUtil.WIDTH, ImageUtil.HIGHT));
        }

        userModel = new ListDataModel(users);
        return "invitePage";
    }

    public void inviteAll() {
        this.team = registrationService.inviteMembersToTeam(team.getId(), users);
        users.clear();
        userModel = new ListDataModel(users);
    }

    public String invite() {
        try {
            Users user = userModel.getRowData();
            List<Users> userInvited = new ArrayList<>();
            userInvited.add(user);
            this.team = registrationService.inviteMembersToTeam(team.getId(), userInvited);
            users.remove(user);
            userModel = new ListDataModel(users);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
