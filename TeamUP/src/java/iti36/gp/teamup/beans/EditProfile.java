/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.beans;

import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.service.dao.imp.RegistrationService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Hend
 */
@ManagedBean
@SessionScoped
public class EditProfile implements Serializable {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    RegistrationService registrationService;
    @ManagedProperty("#{fileUploadView}")
    private FileUploadView file;

    @PostConstruct
    public void init() {
        registrationService = new RegistrationService();

    }

    public EditProfile() {

    }

    public FileUploadView getFile() {
        return file;
    }

    public void setFile(FileUploadView file) {
        this.file = file;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public String editUserData() {
//       FacesContext fc = FacesContext.getCurrentInstance();
//       ExternalContext externalContext = fc.getExternalContext();

        Users currentUser = loginBean.getCurrentUser();
        currentUser.setProfilePicture(file.getFile().getContents());
        Users updateProfile = registrationService.updateProfile(currentUser);
        System.out.println("Succesful Update ");

        return "userLoggedIn";
    }

//public StreamedContent getImage() throws IOException {
//        FacesContext context = FacesContext.getCurrentInstance();
//        String id = context.getExternalContext().getRequestParameterMap().get("id");
//
//        return new DefaultStreamedContent(new ByteArrayInputStream(loginBean.getCurrentUser().getProfilePicture()));
//    }

}
