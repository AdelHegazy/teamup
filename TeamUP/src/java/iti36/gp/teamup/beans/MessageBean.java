/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.beans;

import iti36.gp.teamup.constant.TeamMemberStatusConstant;
import iti36.gp.teamup.constant.TeamMemberTypeConstant;
import iti36.gp.teamup.dto.UserStatusDto;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.service.dao.imp.RegistrationService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

@ManagedBean(name = "messageBean")
@SessionScoped
public class MessageBean implements Serializable {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    RegistrationService registrationService;
    private DataModel<Users> userModel;
    List<Users> chatMembers;
    private Users selectedUser;
    List<Users> membersOfTeamX;
    private Team team;
    private TeamDetailsBean teambeandetail;

    private DataModel<String> messagesModel;

    //  SendAndGetMessagesHistory messageOperationObject;
    Users currentUser;
    String messageTemp;

    public MessageBean() {
    }

    @PostConstruct
    public void init() {
        currentUser = loginBean.getCurrentUser();
        registrationService = new RegistrationService();

    }

    public Users getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(Users selectedUser) {
        this.selectedUser = selectedUser;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public TeamDetailsBean getTeambeandetail() {
        return teambeandetail;
    }

    public void setTeambeandetail(TeamDetailsBean teambeandetail) {
        this.teambeandetail = teambeandetail;
    }

    public DataModel<String> getMessagesModel() {
        return messagesModel;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public void setMessagesModel(DataModel<String> messagesModel) {
        this.messagesModel = messagesModel;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public DataModel<Users> getUserModel() {
        return userModel;
    }

    public void setUserModel(DataModel<Users> userModel) {
        this.userModel = userModel;
    }

    public String getMessageTemp() {
        return messageTemp;
    }

    public void setMessageTemp(String messageTemp) {
        this.messageTemp = messageTemp;
    }

    public DataModel<Users> getAllMembersForChat() {
        try {

            List<Users> chatMembers = registrationService.getAllUsersForChat(currentUser.getId());
            userModel = new ListDataModel((List) chatMembers);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return userModel;

    }

    public DataModel<String> getAllMessagesHistory() {
        try {

            List<String> messagesHistory = registrationService.getAllMessagesBetween2Users(currentUser.getId(), selectedUser.getId());
            messagesModel = new ListDataModel(messagesHistory);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return messagesModel;

    }

    public void sendMessage() {
        String name = loginBean.getCurrentUser().getName();
        registrationService.sendMessage(name + ":" + messageTemp, currentUser.getId(), selectedUser.getId());
        messageTemp="";
        getAllMessagesHistory();
    }

    public void display() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message Sent Succesfully ", "");
        FacesContext.getCurrentInstance().addMessage(messageTemp, message);
    }

    public void OwnersendMessageToAllTeam(List<UserStatusDto> users) {
        String name = loginBean.getCurrentUser().getName();

        for (UserStatusDto user : users) {

            registrationService.sendMessage(name + ":" + messageTemp, currentUser.getId(), user.getUser().getId());

        }

    }public String  renderPage(){
    
            getAllMembersForChat();
            return "messages";
    }

}
