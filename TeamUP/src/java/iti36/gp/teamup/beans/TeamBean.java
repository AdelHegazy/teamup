/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.beans;

import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.service.dao.imp.RegistrationService;
import iti36.gp.teamup.util.ImageUtil;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author adel
 */
@ManagedBean
@SessionScoped
public class TeamBean {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    private RegistrationService registrationService;
    private List<Team> teamAsOwner;
    private List<Team> teamAsMember;
    private DataModel<Team> teamAsOwnerModel;
    private DataModel<Team> teamAsMemberModel;
    private List<Users> inviteUser;
    @ManagedProperty(value = "#{createTeamBean}")
    private CreateTeamBean createTeamBean;

    @PostConstruct
    public void init() {
        registrationService = new RegistrationService();
    }

    public CreateTeamBean getCreateTeamBean() {
        return createTeamBean;
    }

    public void setCreateTeamBean(CreateTeamBean createTeamBean) {
        this.createTeamBean = createTeamBean;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public RegistrationService getRegistrationService() {
        return registrationService;
    }

    public void setRegistrationService(RegistrationService registrationService) {
        this.registrationService = registrationService;

    }

    public List<Team> getTeamAsOwner() {
        return teamAsOwner;
    }

    public void setTeamAsOwner(List<Team> teamAsOwner) {
        this.teamAsOwner = teamAsOwner;
    }

    public List<Team> getTeamAsMember() {
        return teamAsMember;
    }

    public void setTeamAsMember(List<Team> teamAsMember) {
        this.teamAsMember = teamAsMember;
    }

    public DataModel<Team> getTeamAsOwnerModel() {
        return teamAsOwnerModel;
    }

    public void setTeamAsOwnerModel(DataModel<Team> teamAsOwnerModel) {
        this.teamAsOwnerModel = teamAsOwnerModel;
    }

    public DataModel<Team> getTeamAsMemberModel() {
        return teamAsMemberModel;
    }

    public void setTeamAsMemberModel(DataModel<Team> teamAsMemberModel) {
        this.teamAsMemberModel = teamAsMemberModel;
    }

    public List<Users> getInviteUser() {
        return inviteUser;
    }

    public void setInviteUser(List<Users> inviteUser) {
        this.inviteUser = inviteUser;
    }

    public DataModel<Team> getMyTeamAsOwner() {

        teamAsOwner = registrationService.getMyTeamAsOwner(loginBean.getCurrentUser().getId());
        for (Team team : teamAsOwner) {
            team.setImageBase64(ImageUtil.encodeImage(team.getImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));
        }
        teamAsOwnerModel = new ListDataModel(teamAsOwner);
        return teamAsOwnerModel;
    }

    public DataModel<Team> getMyTeamAsMember() {
        teamAsMember = registrationService.getMyTeamAsMember(loginBean.getCurrentUser().getId());
        for (Team team : teamAsMember) {
            team.setImageBase64(ImageUtil.encodeImage(team.getImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));
        }
        teamAsMemberModel = new ListDataModel(teamAsMember);
        return teamAsMemberModel;

    }

    public String inviteMoreMemberToTeam(Team team) {
        inviteUser = registrationService.inviteMoreMemberToTeam(team.getId());
        for (Users user : inviteUser) {
            user.setProfilePictureBase64(ImageUtil.encodeImage(user.getProfilePicture(), ImageUtil.WIDTH, ImageUtil.HIGHT));
        }
        createTeamBean.setUsers(inviteUser);
        DataModel userModel = new ListDataModel(inviteUser);
        createTeamBean.setUserModel(userModel);
        createTeamBean.setTeam(team);
        return "invitePage";
    }
    
    public String renderPage() {
        getMyTeamAsOwner();
        getMyTeamAsMember();
        return "teams.xhtml";
    }
}
