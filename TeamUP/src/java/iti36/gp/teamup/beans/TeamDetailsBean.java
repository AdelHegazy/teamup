/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.beans;

import iti36.gp.teamup.constant.TeamMemberStatusConstant;
import iti36.gp.teamup.constant.TeamMemberTypeConstant;
import iti36.gp.teamup.dto.UserStatusDto;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.service.dao.imp.RegistrationService;
import iti36.gp.teamup.util.ImageUtil;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
/**
 *
 * @author adel
 */
@ManagedBean
@SessionScoped
public class TeamDetailsBean implements Serializable{

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    private RegistrationService registrationService;
    private Team team;
    private List<UserStatusDto> users;
    private int teamLength = 0;
    private boolean owner = false;
    private String teamStatus;

    @PostConstruct
    public void init() {
        registrationService = new RegistrationService();
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public List<UserStatusDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserStatusDto> users) {
        this.users = users;
    }

    public int getTeamLength() {
        return teamLength;
    }

    public void setTeamLength(int teamLength) {
        this.teamLength = teamLength;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }

    public String getTeamStatus() {
        return teamStatus;
    }

    public void setTeamStatus(String teamStatus) {
        this.teamStatus = teamStatus;
    }

    public String renderTeamDetails(int teamId) {
        team = registrationService.getTeam(teamId);
        team.setImageBase64(ImageUtil.encodeImage(team.getImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));
        teamStatus = team.getTeamStatus().getStatus();
        users = new ArrayList<>();
        UserStatusDto userStatusDto;
        owner = false;
        for (Object object : team.getUserHasTeams()) {
            UserHasTeam userHasTeam = (UserHasTeam) object;
            if (loginBean.getCurrentUser().getId() != userHasTeam.getUsers().getId()
                    && userHasTeam.getTeamMemberType().getId() == TeamMemberTypeConstant.MEMBER_ID) {
                userStatusDto = new UserStatusDto();
                Users user = userHasTeam.getUsers();
                user.setProfilePictureBase64(ImageUtil.encodeImage(user.getProfilePicture(), ImageUtil.WIDTH, ImageUtil.HIGHT));
                userStatusDto.setUser(user);
                userStatusDto.setTeamMemberStatus(userHasTeam.getTeamMemberStatus());
                if (userHasTeam.getTeamMemberStatus().getMemberStatus().equals(TeamMemberStatusConstant.CONFIRM)) {
                    teamLength++;
                }
                users.add(userStatusDto);
            } else if (loginBean.getCurrentUser().getId().equals(userHasTeam.getUsers().getId())
                    && userHasTeam.getTeamMemberType().getId() == TeamMemberTypeConstant.OWNER_ID) {
                owner = true;
            }
        }
        return "teamDetails";
    }

    public void changeStatus(boolean status) {
        team = registrationService.changeTeamStatus(team, status);
        teamStatus = team.getTeamStatus().getStatus();
    }
}
