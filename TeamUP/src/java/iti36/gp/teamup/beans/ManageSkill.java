package iti36.gp.teamup.beans;

import iti36.gp.teamup.Constant.SkillsStatusConstant;
import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.UserHasSkill;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.service.dao.imp.RegistrationService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author adel
 */
@ManagedBean(name = "bean")
@SessionScoped
public class ManageSkill {

    private static List<String> selected = new ArrayList<>();
    private final Map<String, Map<String, String>> data = new HashMap<>();
    private String category;
    private String skill;
    private Map<String, String> categories;
    private Map<String, String> skills;
    @ManagedProperty("#{categoryBean}")
    private CategoryBean categoryBean;
    private RegistrationService registrationService;
    private boolean show = false;
    private Map<String, Integer> categoriesId;
    private Map<String, Integer> skillId;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    private String categoryForNewSkill;
    private String newSkill;
    private List<String> userSkills = new ArrayList<>();

    @PostConstruct
    public void init() {
//        getSkillCategory();
////        displayUserSkill();
//        getUserSkill();
    }

    public void getSkillCategory() {
        registrationService = new RegistrationService();
        List<SkillCategory> ob = CategoryBean.getCategory();
        categories = new HashMap<String, String>();
        categoriesId = new HashMap<String, Integer>();
        skillId = new HashMap<String, Integer>();
        Map<String, String> skillMap = new HashMap<>();
        for (SkillCategory skillCategory : ob) {
            categories.put(skillCategory.getName(), skillCategory.getName());
            categoriesId.put(skillCategory.getName(), skillCategory.getId());
            for (Object skillObject : skillCategory.getSkillses()) {
                Skills selectedSkill = (Skills) skillObject;
                if (selectedSkill.getSkillsStatus().getStatusName().equals(SkillsStatusConstant.ACTIVATED)) {
                    skillMap.put(selectedSkill.getName(), selectedSkill.getName());
                    skillId.put(selectedSkill.getName(), selectedSkill.getId());
                }
            }
            data.put(skillCategory.getName(), skillMap);
            skillMap = new HashMap<>();
        }
    }

    public void getUserSkill() {
        userSkills.clear();
        for (Object object : loginBean.getCurrentUser().getUserHasSkills()) {
            UserHasSkill skill = (UserHasSkill) object;
            if (skill.getSkills().getSkillsStatus().getId() == SkillsStatusConstant.ACTIVATED_ID) {
                userSkills.add(skill.getSkills().getName());
            }
        }
    }

    public void submit() {
        skill = categoryBean.getSkillName();
        if (!userSkills.contains(skill)) {
            selected.add(skill);
        }
    }

    public void remove(String input) {
        selected.remove(input);
    }

    public void delete(String input) {
        selected.remove(input);
    }

    public List<String> getUserSkills() {
        return userSkills;
    }

    public void setUserSkills(List<String> userSkills) {
        this.userSkills = userSkills;
    }

    public Map<String, Map<String, String>> getData() {
        return data;
    }

    public List<String> getSelected() {
        return selected;
    }

    public void setSelected(List<String> selected) {
        this.selected = selected;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public Map<String, String> getCategories() {
        return categories;
    }

    public void setCategories(Map<String, String> categories) {
        this.categories = categories;
    }

    public Map<String, String> getSkills() {
        return skills;
    }

    public void setSkills(Map<String, String> skills) {
        this.skills = skills;
    }

    public String getNewSkill() {
        return newSkill;
    }

    public void setNewSkill(String newSkill) {
        this.newSkill = newSkill;
    }

    public String getCategoryForNewSkill() {
        return categoryForNewSkill;
    }

    public void setCategoryForNewSkill(String categoryForNewSkill) {
        this.categoryForNewSkill = categoryForNewSkill;
    }

    public CategoryBean getCategoryBean() {
        return categoryBean;
    }

    public void setCategoryBean(CategoryBean categoryBean) {
        this.categoryBean = categoryBean;
    }

    public RegistrationService getRegistrationService() {
        return registrationService;
    }

    public void setRegistrationService(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    public void onCategoryChanged() {
        if (category != null && !category.equals("")) {
            skills = data.get(category);
        } else {
            skills = new HashMap<>();
        }
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public void showPanel() {
        if (show) {
            show = false;
        } else {
            show = true;
        }
    }

    public void addSkill() {
        try {
            List<Integer> skillListId = new ArrayList<>();
            for (int i = 0; i < selected.size(); i++) {
                skillListId.add(skillId.get(selected.get(i)));
            }
            Users user = registrationService.addSkill(loginBean.getCurrentUser().getId(), skillListId, 1);
            if (user != null) {
                loginBean.setCurrentUser(user);
                selected.clear();
                getUserSkill();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public void addNewSkill() {
        try {
            Users user = registrationService.addNewSkill(loginBean.getCurrentUser().getId(), categoriesId.get(categoryForNewSkill), newSkill);
            if (user != null) {
                loginBean.setCurrentUser(user);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void deleteSkill(String input) {
        int idSkill = skillId.get(input);
        Users user = registrationService.removeUserSkill(loginBean.getCurrentUser().getId(), idSkill);
        if (user != null) {
            loginBean.setCurrentUser(user);
            userSkills.remove(input);
        }

    }
    public String render() {
        getSkillCategory();
        getUserSkill();
        return "manage.xhtml";
    }
}
