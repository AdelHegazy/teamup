/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.beans;

import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.service.dao.imp.RegistrationService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Hend
 */
@ManagedBean(name = "signupbean")
@SessionScoped
public class SignUp implements Serializable {

    RegistrationService registrationService;
    Users user = null;
    @ManagedProperty("#{fileUploadView}")
    private FileUploadView file;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;

    public SignUp() {
    }

    @PostConstruct
    public void init() {
        user = new Users();
        registrationService = new RegistrationService();
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public FileUploadView getFile() {
        return file;
    }

    public void setFile(FileUploadView file) {
        this.file = file;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    
    public String addUser() {

        try {
            user.setProfilePicture(file.getFile().getContents());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Users signUpUser = registrationService.registration(user);
            loginBean.setCurrentUser(user);
            LoginBean.isLogged=true;
            System.out.println(signUpUser.getEmail());
            return "userLoggedIn";
        }
    }

    public void validateEmail(FacesContext fc, UIComponent c, Object value) {

        List<Users> users = registrationService.getAllUsers();
        for (Users u : users) {
            if (((String) value).equals(u.getEmail())) {
                throw new ValidatorException(new FacesMessage("Email   is already used "));
            }
        }

    }
}
