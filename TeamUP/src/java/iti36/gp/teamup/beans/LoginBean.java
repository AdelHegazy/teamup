/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.beans;

import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.service.dao.imp.RegistrationService;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author AhmedElGazzar
 */
@ManagedBean
@SessionScoped
public class LoginBean implements java.io.Serializable {

    public static boolean isLogged = false;
    RegistrationService registrationService;
    private String email;
    private String password;
    private Users currentUser;

    @PostConstruct
    public void init() {
        registrationService = new RegistrationService();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Users getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Users currentUser) {
        this.currentUser = currentUser;
    }

    public String login() {
        Users user = new Users();
        user.setPassword(password);
        user.setEmail(email);
        Users login = registrationService.login(user);
        if (login == null) {
            return "index";
        } else {
            currentUser = login;
            isLogged = true;
            return "userLoggedIn";
        }
    }
        public void logout() {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.invalidateSession();
            ec.redirect(ec.getRequestContextPath() + "/index.xhtml");
            isLogged = false;
        } catch (IOException ex) {
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String refreshUser() {
        Users user = registrationService.getUser(currentUser.getId());
        if (user != null) {
            registrationService.getUser(currentUser.getId());
            currentUser = user;
        }
        return "profile.xhtml";
    }
}
