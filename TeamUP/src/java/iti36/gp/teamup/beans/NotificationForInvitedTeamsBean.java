/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.beans;

import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.service.dao.imp.RegistrationService;
import iti36.gp.teamup.util.ImageUtil;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Hend
 */
@ManagedBean(name = "notifyBean")
@SessionScoped
public class NotificationForInvitedTeamsBean implements Serializable {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    RegistrationService registrationService;
    private String accept = "accept";
    private String reject = "reject";
    private List<Team> invitedTeamsOfUSer;
    private DataModel<Team> teamModel;
    private List<UserHasTeam> userHasTeam;
    private DataModel<UserHasTeam> userHasTeamModel;
    private   int count = 0;
    private   int oldCount =0;
     private   int tempCount =0;
     

    @PostConstruct
    public void init() {
        registrationService = new RegistrationService();
    }

    public NotificationForInvitedTeamsBean() {
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }

    public String getReject() {
        return reject;
    }

    public void setReject(String reject) {
        this.reject = reject;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<Team> getInvitedTeamsOfUSer() {
        return invitedTeamsOfUSer;
    }

    public void setInvitedTeamsOfUSer(List<Team> invitedTeamsOfUSer) {
        this.invitedTeamsOfUSer = invitedTeamsOfUSer;
    }

    public DataModel<Team> getTeamModel() {
        return teamModel;
    }

    public void setTeamModel(DataModel<Team> teamModel) {
        this.teamModel = teamModel;
    }

    public List<UserHasTeam> getUserHasTeam() {
        return userHasTeam;
    }

    public void setUserHasTeam(List<UserHasTeam> userHasTeam) {
        this.userHasTeam = userHasTeam;
    }

    public DataModel<UserHasTeam> getUserHasTeamModel() {
        return userHasTeamModel;
    }

    public void setUserHasTeamModel(DataModel<UserHasTeam> userHasTeamModel) {
        this.userHasTeamModel = userHasTeamModel;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getOldCount() {
        return oldCount;
    }

    public void setOldCount(int oldCount) {
        this.oldCount = oldCount;
    }

    public void  notigyInvitedTeams() {
        Users currentUser = loginBean.getCurrentUser();
        System.out.println("Succesful Teams  ");
        invitedTeamsOfUSer = registrationService.getInvitedTeamsOfUSerService(currentUser);
        for (Team team : invitedTeamsOfUSer) {
            team.setImageBase64(ImageUtil.encodeImage(team.getImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));
        }
        tempCount = tempCount + invitedTeamsOfUSer.size();
        teamModel = new ListDataModel(invitedTeamsOfUSer);
    }
 
    public String operationOnInvitation(int teamId, String operation) {
        try {
            Users user;
            Object o = (Object) teamModel.getRowData();
             Team VALUE = (Team)o;
            if (operation.equals(accept)) {
                user = registrationService.operationOnInvitation(teamId, loginBean.getCurrentUser().getId(), true);
            } else {
                user = registrationService.operationOnInvitation(teamId, loginBean.getCurrentUser().getId(), false);
            }
            loginBean.setCurrentUser(user);
            invitedTeamsOfUSer.remove(VALUE);
            teamModel = new ListDataModel(invitedTeamsOfUSer);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void getUserWhoAcceptInvtation() {
        Users currentUser = loginBean.getCurrentUser();
        System.out.println("Succesful Teams  ");
        userHasTeam = registrationService.getUserWhoAcceptInvtation(currentUser.getId());
        for (UserHasTeam team : userHasTeam) {
           team.getTeam().setImageBase64(ImageUtil.encodeImage(team.getTeam().getImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));
            
        }
        tempCount = tempCount + userHasTeam.size();
        userHasTeamModel = new ListDataModel(userHasTeam);
    }


    public String confirm() {
        try {
            Object object = (Object) userHasTeamModel.getRowData();
            UserHasTeam team = (UserHasTeam) object;
            userHasTeam.remove(team);
            userHasTeamModel = new ListDataModel(userHasTeam);
            registrationService.confirm(team.getId(), true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    public String renderPage() {
        oldCount=tempCount;
        notigyInvitedTeams();
        getUserWhoAcceptInvtation();
        tempCount=0;
        count=0;
        return "Notification.xhtml";
    }

    public void getData() {
        tempCount = 0;
        notigyInvitedTeams();
        getUserWhoAcceptInvtation();
        if (tempCount < oldCount) {
            oldCount = tempCount;
        }
        count = tempCount - oldCount;
    }
}
