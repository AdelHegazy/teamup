/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.beans;

import iti36.gp.teamup.dto.UserStatusDto;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import java.io.Serializable;

/**
 *
 * @author adel
 */
@ManagedBean
@SessionScoped
public class ImageBean implements Serializable {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{teamDetailsBean}")
    private TeamDetailsBean teamDetails;
    @ManagedProperty(value = "#{createTeamBean}")
    private CreateTeamBean createTeamBean;

    public TeamDetailsBean getTeamDetails() {
        return teamDetails;
    }

    public void setTeamDetails(TeamDetailsBean teamDetails) {
        this.teamDetails = teamDetails;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public CreateTeamBean getCreateTeamBean() {
        return createTeamBean;
    }

    public void setCreateTeamBean(CreateTeamBean createTeamBean) {
        this.createTeamBean = createTeamBean;
    }

    public StreamedContent getImage() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            String teamId = context.getExternalContext().getRequestParameterMap().get("id");
            String memberId = context.getExternalContext().getRequestParameterMap().get("memberId");
            String userId = context.getExternalContext().getRequestParameterMap().get("userId");
            if (teamId != null && memberId == null && userId == null) {
                UserHasTeam team = null;
                int value = Integer.parseInt(teamId);
                for (Object object : loginBean.getCurrentUser().getUserHasTeams()) {
                    team = (UserHasTeam) object;
                    if (value == team.getTeam().getId()) {
                        break;
                    }
                }
                return new DefaultStreamedContent(new ByteArrayInputStream(team.getTeam().getImage()));
            } else if (teamId == null && memberId != null && userId == null) {
                UserStatusDto userDto = null;
                int value = Integer.parseInt(memberId);
                for (Object object : teamDetails.getUsers()) {
                    userDto = (UserStatusDto) object;
                    if (memberId != null && value == userDto.getUser().getId()) {
                        break;
                    }
                }
                return new DefaultStreamedContent(new ByteArrayInputStream(userDto.getUser().getProfilePicture()));
            } else if (teamId == null && memberId == null && userId != null) {
                Users user = null;
                int value = Integer.parseInt(userId);
                for (Users users : createTeamBean.getUsers()) {
                    user = users;
                    if (value == user.getId()) {
                        break;
                    }
                }
                return new DefaultStreamedContent(new ByteArrayInputStream(user.getProfilePicture()));
            } else {
                return new DefaultStreamedContent();
            }
        }
    }
}
