/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.gcm;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author adel
 */
public class GCMNotification {

    // Put your Google API Server Key here
    private static final String GOOGLE_SERVER_KEY = "AIzaSyAGyzdTu01TDQSeoQB6R9ru79HPMYBfuCE";
    static final String MESSAGE_KEY = "message";
    private static List<String> regIdList = new ArrayList();

    public  void sendNotification(String message) {
        try {
            Sender sender = new Sender(GOOGLE_SERVER_KEY);
            Message messageObject = new Message.Builder().timeToLive(30)
                    .delayWhileIdle(true).addData(MESSAGE_KEY, message)
                    .build();
            sender.send(messageObject, regIdList, 1);
        } catch (IOException ex) {
            Logger.getLogger(GCMNotification.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public  void registration(String regId) {
        regIdList.add(regId);
    }

    public  void clear() {
        regIdList.clear();
    }
}
