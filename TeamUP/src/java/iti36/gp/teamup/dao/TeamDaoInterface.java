/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao;

import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.TeamHasSkill;
import iti36.gp.teamup.entity.Users;
import java.util.List;

/**
 *
 * @author AhmedElGazzar
 */
public interface TeamDaoInterface {

    public Team getTeamById(int id);

    public boolean deleteUserFromTeam(int userId, int teamId);

    public Team createTeam(TeamHasSkill team,int owner); 

    public Team updateTeam(Team team); 

    public List<Team> getAllTeam(Users user);
    
    public Team changeTeamStatus(Team team, boolean cancel);

}
