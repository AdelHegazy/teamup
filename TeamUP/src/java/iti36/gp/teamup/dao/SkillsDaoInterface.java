/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao;

import iti36.gp.teamup.entity.Users;
import java.util.List;

/**
 *
 * @author AhmedElGazzar
 */
public interface SkillsDaoInterface {

    public Users addSkillToUser(int userId, List<Integer> skill, int level);

    public Users addNewSkill(int userId, int categoryId, String skillName);
    
    public Users removeUserSkill(int userId, int skillId);
}
