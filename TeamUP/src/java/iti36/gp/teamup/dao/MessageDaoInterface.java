/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao;

import iti36.gp.teamup.entity.MessageHasUsers;
import iti36.gp.teamup.entity.Users;
import java.util.List;

/**
 *
 * @author adel
 */
public interface MessageDaoInterface {

    public List<String> getAllMessagesBetween2Users(int userLoggedIn, int UserSelected);

    public List<MessageHasUsers> getListOfUsersHasMessage(int to, int from);

    public boolean sendMessage(String message, int from, int to);
}
