/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao;

import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.UserHasSkill;
import iti36.gp.teamup.entity.Users;
import java.util.List;

/**
 *
 * @author adel
 */
public interface UserDaoInterface {

    public Users signUp(Users user);
   
    public Users updateProfile(Users user); 
    
    public Users login(Users user);
    
    public Users getUser(int id);
    
    public List<UserHasSkill> getUserHasSpecificSkill(int skillId);
}
