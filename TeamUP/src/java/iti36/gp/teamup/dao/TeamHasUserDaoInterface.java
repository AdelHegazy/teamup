/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao;

import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import java.util.List;

/**
 *
 * @author adel
 */
public interface TeamHasUserDaoInterface {

    public Team inviteMemberToTeam(int teamId, List<Users> users);

    public UserHasTeam updateTeam(UserHasTeam user);

    public List<UserHasTeam> getUserWhoAcceptInvtation(int userId);

    public UserHasTeam confirm(int userHasTeamId, boolean confirm);

    public List<Team> getMyTeamAsOwner(int userId);
    
    public List<Team> getMyTeamAsMember(int userId);

    public List<Users> inviteMoreMemberToTeam(int teamId);

    public Users getTeamOwner(Team team);
   
   public int getTeamHasUserId(Users user, Team team, String type);
    
}
