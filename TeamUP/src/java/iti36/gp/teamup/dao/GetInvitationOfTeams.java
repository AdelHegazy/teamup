/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao;

import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.TeamMemberStatus;
import iti36.gp.teamup.entity.Users;
import java.util.List;

/**
 *
 * @author Hend
 */
public interface GetInvitationOfTeams {
    
    public List<Team> getInvitedTeamsOfUSer(Users user );

}
