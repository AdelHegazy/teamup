/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao.imp;

import iti36.gp.teamup.constant.TeamMemberStatusConstant;
import iti36.gp.teamup.constant.TeamMemberTypeConstant;
import iti36.gp.teamup.constant.TeamStatusConstant;
import iti36.gp.teamup.dao.TeamDaoInterface;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.TeamHasSkill;
import iti36.gp.teamup.entity.TeamMemberStatus;
import iti36.gp.teamup.entity.TeamMemberType;
import iti36.gp.teamup.entity.TeamStatus;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import iti36.gp.teamup.util.DbOpenSession;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.hibernate.Session;

/**
 *
 * @author AhmedElGazzar
 */
public class TeamDao extends CommonOperationImp implements TeamDaoInterface {

    @Override
    public Team getTeamById(int id) {

        Team team = get(Team.class, id);
        return team;
    }

    @Override
    public boolean deleteUserFromTeam(int userId, int teamId) {
        Team team = new Team();
        Users user = get(Users.class, userId);
        team.getUserHasTeams().remove(user);
        System.out.println("user deleted");
        return true;
    }

    @Override
    public Team createTeam(TeamHasSkill teamHasSkill, int owner) {
        try {
            UserHasTeam userHasTeam = new UserHasTeam();
            UserDao userDao = new UserDao();
            TeamStatus teamStatus = get(TeamStatus.class, TeamStatusConstant.ACTIVE_ID);
            Team team = teamHasSkill.getTeam();
            team.setTeamStatus(teamStatus);
            userHasTeam.setTeam(team);
            userHasTeam.setUsers(userDao.getUser(owner));
            TeamMemberStatus teamOwner = get(TeamMemberStatus.class, TeamMemberStatusConstant.ACCEPT_ID);
            userHasTeam.setTeamMemberStatus(teamOwner);
            TeamMemberType teamMemberType = get(TeamMemberType.class, TeamMemberTypeConstant.OWNER_ID);
            userHasTeam.setTeamMemberType(teamMemberType);
            add(team);
            add(userHasTeam);
            add(teamHasSkill);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return teamHasSkill.getTeam();
    }

    @Override
    public Team updateTeam(Team team) {

        return update(team);

    }

    @Override
    public List<Team> getAllTeam(Users user) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        user = (Users) session.get(Users.class, user.getId());
        List<Team> teamsOfUser = new ArrayList<>();
        for (Iterator<UserHasTeam> iterator = user.getUserHasTeams().iterator(); iterator.hasNext();) {
            UserHasTeam userHasTeam = iterator.next();
            teamsOfUser.add(userHasTeam.getTeam());
        }

//        session.close();
        return teamsOfUser;

    }

    @Override
    public Team changeTeamStatus(Team team, boolean cancel) {
        TeamStatus teamStatus = null;
        if (cancel) {
            teamStatus = get(TeamStatus.class, TeamStatusConstant.CANCELLED_ID);
        } else {
            teamStatus = get(TeamStatus.class, TeamStatusConstant.ACTIVE_ID);
        }
        team.setTeamStatus(teamStatus);
        return updateTeam(team);
    }

}
