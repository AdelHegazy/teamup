/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao.imp;

import iti36.gp.teamup.Constant.SkillsStatusConstant;
import iti36.gp.teamup.dao.AdminDaoInterface;
import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.SkillsStatus;
import iti36.gp.teamup.entity.TeamHasSkill;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import iti36.gp.teamup.util.DbOpenSession;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author AhmedElGazzar
 */
public class AdminDao extends CommonOperationImp implements AdminDaoInterface {

    private static final String ADMIN_LOGIN = "FROM Users u WHERE u.email =? AND u.password =? ";
    private static final String GETWAITINGSKILLS = "FROM Skills s WHERE s.skillsStatus = 3 ";
    private static final String ACTIVESKILLSSTATUS = "UPDATE Skills s SET skillsStatus=1 WHERE s.id=:id";
    private static final String DEAVTIVESKILLSSTATUS = "UPDATE Skills s SET skillsStatus=2 WHERE s.id=:id";
    private static final String GETTEAMREPORT = "FROM UserHasTeam u GROUP BY u.team ORDER BY u.teamMemberStatus";
    private static final String GETTEAMSkills = "SELECT teamHasSkills FROM Team t WHERE t.id =:teamId";
    private static final String GETTEAMMEMBERSFROMUSERHASTEAM = "SELECT userHasTeams FROM Team u WHERE u.id =:teamId";
    private static final String GETCOUNTOFWAITINGSKILLS = "SELECT count(skillsStatus)FROM Skills s WHERE s.skillsStatus = 3 ";
    private static final String GETSKILLSNAME = "FROM Skills s WHERE s.name =? ";
    private static final String GETCATEGORYNAME = "FROM SkillCategory s WHERE s.name =? ";

    Users user;
    SkillsStatus status;

    @Override
    public Users adminLogin(String email, String password) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.getTransaction().begin();

        Query query = session.createQuery(ADMIN_LOGIN).setString(0, email).setString(1, password);
        List l = query.list();
        if (l.size() > 0) {
            user = (Users) l.get(0);
            System.out.println("logged admin :" + user.getEmail());
            return user;
        } else {
            System.out.println("is not logged admin");
            return null;
        }
    }

    @Override
    public List<Skills> getWaitingSkills() {
        List<Skills> data = getData(GETWAITINGSKILLS);
        return data;
    }

    @Override
    public List<Skills> getCountOfWaitingSkills() {
        System.out.println("in GETCOUNTOFWAITINGSKILLS");
        List<Skills> data = getData(GETCOUNTOFWAITINGSKILLS);
        return data;
    }

    @Override
    public void acvtiveSkills(int skillsId) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.getTransaction().begin();

        Query query = session.createQuery(ACTIVESKILLSSTATUS);
        query.setParameter("id", skillsId);

        int result = query.executeUpdate();
        System.out.println(result + " records updated");

        session.getTransaction().commit();
    }

    @Override
    public void deacvtiveSkills(int skillsId) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.getTransaction().begin();
        Query query = session.createQuery(DEAVTIVESKILLSSTATUS);
        query.setParameter("id", skillsId);

        int result = query.executeUpdate();
        System.out.println(result + " records updated");

        session.getTransaction().commit();
    }

    @Override
    public List<Skills> getAllSkills() {
        return getAll(Skills.class);
    }

    @Override
    public void addSkill(int categoryId, String skill) {

        if (!validateSkill(skill)) {
            SkillCategory category = get(SkillCategory.class, categoryId);

            SkillsStatus ss = new SkillsStatus(SkillsStatusConstant.ACTIVATED_ID, SkillsStatusConstant.ACTIVATED);

            Skills s = new Skills(category, ss, skill);

            add(s);
        } else {
            System.out.println("skills is exiting");
        }

    }

    @Override
    public List<SkillCategory> getAllSkillCategory() {
        return getAll(SkillCategory.class);
    }

    @Override
    public void addCategory(String cateName) {
        if (!validateSkillCategory(cateName)) {
            SkillCategory sc = new SkillCategory(cateName);
            add(sc);
        } else {
            System.out.println("category is exist");
        }
    }

    @Override
    public List<UserHasTeam> getTeamReport() {
        List<UserHasTeam> hasTeams = getData(GETTEAMREPORT);
        return hasTeams;
    }

    @Override
    public List<UserHasTeam> getTeamMembersByUser(int teamId) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.getTransaction().begin();
        System.out.println("in session");
        Query query = session.createQuery(GETTEAMMEMBERSFROMUSERHASTEAM);
        System.out.println("in query : " + query);
        List<UserHasTeam> list = query.setParameter("teamId", teamId).list();
        return list;

    }

    @Override
    public List<TeamHasSkill> getTeamSkills(int teamId) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.getTransaction().begin();
        System.out.println("in session");
        Query query = session.createQuery(GETTEAMSkills);
        System.out.println("in query : " + query);
        List<TeamHasSkill> list = query.setParameter("teamId", teamId).list();
        return list;
    }

    @Override
    public boolean validateSkill(String name) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.getTransaction().begin();

        Query query = session.createQuery(GETSKILLSNAME).setString(0, name);
        List l = query.list();
        if (l.size() > 0) {
            Skills s = (Skills) l.get(0);
            System.out.println("Skill name is :" + s.getName());
            return true;
        } else {
            System.out.println("is not logged admin");
            return false;
        }

    }

    @Override
    public boolean validateSkillCategory(String name) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.getTransaction().begin();

        Query query = session.createQuery(GETCATEGORYNAME).setString(0, name);
        List l = query.list();
        if (l.size() > 0) {
            SkillCategory s = (SkillCategory) l.get(0);
            System.out.println("Category name is :" + s.getName());
            return true;
        } else {
            System.out.println("Category is not exist");
            return false;
        }
    }
}
