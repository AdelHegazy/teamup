/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao.imp;

import iti36.gp.teamup.Constant.SkillsStatusConstant;
import iti36.gp.teamup.Constant.UserSkillLevelConstant;
import iti36.gp.teamup.dao.SkillsDaoInterface;
import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.SkillsStatus;
import iti36.gp.teamup.entity.UserHasSkill;
import iti36.gp.teamup.entity.UserSkillLevel;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import iti36.gp.teamup.util.DbOpenSession;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author AhmedElGazzar
 */
public class SkillsDao extends CommonOperationImp implements SkillsDaoInterface {

    @Override
    public Users addSkillToUser(int userId, List<Integer> skills, int levelId) {
        UserHasSkill userHasSkill = null;
        try {
                Users user = get(Users.class, userId);
                UserSkillLevel level = get(UserSkillLevel.class, levelId);
            for (Integer skillId : skills) {
                Skills newSkill = get(Skills.class, skillId);
                userHasSkill = new UserHasSkill();
                userHasSkill.setSkills(newSkill);
                userHasSkill.setUsers(user);
                userHasSkill.setUserSkillLevel(level);
                add(userHasSkill);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (userHasSkill != null) {
            return userHasSkill.getUsers();
        } else {
            return null;
        }
    }

    @Override
    public Users addNewSkill(int userId, int categoryId, String skillName) {
        Users user = null;
        try {
            SkillCategory category = get(SkillCategory.class, categoryId);
            SkillsStatus skillStatus = get(SkillsStatus.class, SkillsStatusConstant.WAITING_ID);
            Skills newSkill = new Skills();
            newSkill.setName(skillName);
            newSkill.setSkillsStatus(skillStatus);
            newSkill.setSkillCategory(category);
            add(newSkill);
            user = get(Users.class, userId);
            UserSkillLevel level = get(UserSkillLevel.class, UserSkillLevelConstant.BEGINNER_ID);
            UserHasSkill userHasSkill = new UserHasSkill();
            userHasSkill.setUsers(user);
            userHasSkill.setSkills(newSkill);
            userHasSkill.setUserSkillLevel(level);
            add(userHasSkill);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public Users removeUserSkill(int userId, int skillId) {
        try {
            Skills skill = get(Skills.class, skillId);
            Users user = get(Users.class, userId);
            Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
            session.getTransaction().begin();
            UserHasSkill userHasSkill = (UserHasSkill) session.createCriteria(UserHasSkill.class)
                    .add(Restrictions.eq("skills", skill)).add(Restrictions.eq("users", user)).uniqueResult();
            for (Object object : user.getUserHasSkills()) {
                UserHasSkill userSkill = (UserHasSkill) object;
                if (userSkill.getId() == userHasSkill.getId()) {
                    user.getUserHasSkills().remove(userSkill);
                    break;
                }
            }           
            session.delete(userHasSkill);
            session.getTransaction().commit();
            session.close();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
