/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao.imp;

import iti36.gp.teamup.entity.Message;
import iti36.gp.teamup.entity.MessageHasUsers;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import iti36.gp.teamup.util.DbOpenSession;
import static java.lang.reflect.Array.set;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Hend
 */
public class Senders extends CommonOperationImp {

    
    public List<Users> getSenderForUser(int id) {

        Users user = get(Users.class, id);
        List<Users> sender = new ArrayList<>();;
        Set messageHasUsersesForFrom = user.getMessageHasUsersesForTo();

        Iterator iterator = messageHasUsersesForFrom.iterator();

        while (iterator.hasNext()) {
            MessageHasUsers messge = (MessageHasUsers) iterator.next();

            sender.add(messge.getUsersByFrom());

        }

        return sender;
    }

    
    /*
    public static void main(String args[]) {

        Senders obj = new Senders();
//        obj.sendMessage("Test Msg V.1", 1, 2);
        List<Users> senderForUser = obj.getSenderForUser(1);
        senderForUser.stream().forEach((a) -> {
            System.out.println("Name  : " + a.getName());
        });
    }*/
}
