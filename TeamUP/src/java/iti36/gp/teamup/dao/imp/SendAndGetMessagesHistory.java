/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao.imp;

import iti36.gp.teamup.dao.MessageDaoInterface;
import iti36.gp.teamup.entity.Message;
import iti36.gp.teamup.entity.MessageHasUsers;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import iti36.gp.teamup.util.DbOpenSession;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Hend
 */
public class SendAndGetMessagesHistory extends CommonOperationImp implements MessageDaoInterface {

    @Override
    public boolean sendMessage(String message, int from, int to) {
        Message messageObject = new Message(message);
        add(messageObject);
        Users usersTo = get(Users.class, to);
        Users usersfrom = get(Users.class, from);
        MessageHasUsers registerMessage = new MessageHasUsers();
        registerMessage.setUsersByTo(usersTo);
        registerMessage.setUsersByFrom(usersfrom);
        registerMessage.setMessage(messageObject);
        add(registerMessage);
        return false;
    }

    public List<MessageHasUsers> getListOfUsersHasMessage(int to, int from) {
        Users X = get(Users.class, to);
        Users Y = get(Users.class, from);
        //List<String> messagesContents = new ArrayList();
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.getTransaction().begin();
        Criteria criteria
                = session.createCriteria(MessageHasUsers.class);

        Criterion rest1
                = Restrictions.and(
                        Restrictions.eq("usersByTo", X),
                        Restrictions.eq("usersByFrom", Y)
                );

        Criterion rest2 = Restrictions.and(
                Restrictions.eq("usersByTo", Y),
                Restrictions.eq("usersByFrom", X)
        );
        criteria.add(Restrictions.or(rest1, rest2));
        List list = (List<MessageHasUsers>) criteria.list();

        return list;

    }

    @Override
    public List<String> getAllMessagesBetween2Users(int userLoggedIn, int UserSelected) {
        List<MessageHasUsers> messageHasUserses = getListOfUsersHasMessage(UserSelected, userLoggedIn);
        List<String> messages = new ArrayList();
        Iterator iterator = messageHasUserses.iterator();
        while (iterator.hasNext()) {

            MessageHasUsers item = (MessageHasUsers) iterator.next();
            messages.add(item.getMessage().getMessageContent());
        }

        return messages;
    }

}
