/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao.imp;

import iti36.gp.teamup.dao.CategoryDaoInterface;
import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import java.util.List;

/**
 *
 * @author adel
 */
public class CategoryDao extends CommonOperationImp implements CategoryDaoInterface{

    @Override
    public List<SkillCategory> getAllCategory() {
        return (List<SkillCategory>) getAll(SkillCategory.class);
    }

}
