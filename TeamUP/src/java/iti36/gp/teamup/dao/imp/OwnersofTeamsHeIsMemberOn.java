/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao.imp;

import iti36.gp.teamup.constant.TeamMemberStatusConstant;
import iti36.gp.teamup.constant.TeamMemberTypeConstant;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.TeamMemberStatus;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 *
 * @author Hend
 */
public class OwnersofTeamsHeIsMemberOn extends CommonOperationImp {
 
    public List<Team> getConfirmedTeamsOfUSer(int id) {

        Users user = get(Users.class, id);
        List<Team> TeamsWithStatusConfirmed = new ArrayList();
        List<TeamMemberStatus> teamMemberStatus = new ArrayList<>();
        for (Iterator<UserHasTeam> iterator = user.getUserHasTeams().iterator(); iterator.hasNext();) {

            UserHasTeam userHasTeam = iterator.next();

            if (userHasTeam.getTeamMemberType()
                    .getMemberType().equals(TeamMemberTypeConstant.MEMBER)
                    && userHasTeam.getTeamMemberStatus()
                    .getMemberStatus()
                    .equals(TeamMemberStatusConstant.CONFIRM)) {
//              
                TeamsWithStatusConfirmed.add(userHasTeam.getTeam());

            }
        }
//       
        return TeamsWithStatusConfirmed;

    }
//====================================================


    public List< Users> getOwnersOfTeams(int id ) {
        
        List<Team> teams = getConfirmedTeamsOfUSer(id);
        
        List<Users> ownsers = new ArrayList();

        for (Team temp : teams) {
            Team team1 = get(Team.class, temp.getId());
            Set userHasTeams = team1.getUserHasTeams();
            Iterator iterator = userHasTeams.iterator();
            while (iterator.hasNext()) {
                UserHasTeam obj = (UserHasTeam) iterator.next();
                if (obj.getTeamMemberType().getMemberType()
                        .equals(TeamMemberTypeConstant.OWNER)) {
                    ownsers.add(obj.getUsers());
                    break;
                }
            }

        }
        return ownsers;

    }

    
    
    
   
}






