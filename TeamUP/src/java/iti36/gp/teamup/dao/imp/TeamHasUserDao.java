/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao.imp;

import iti36.gp.teamup.constant.TeamMemberStatusConstant;
import iti36.gp.teamup.constant.TeamMemberTypeConstant;
import iti36.gp.teamup.dao.TeamHasUserDaoInterface;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.TeamHasSkill;
import iti36.gp.teamup.entity.TeamMemberStatus;
import iti36.gp.teamup.entity.TeamMemberType;
import iti36.gp.teamup.entity.UserHasSkill;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import iti36.gp.teamup.util.DbOpenSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author adel
 */
public class TeamHasUserDao extends CommonOperationImp implements TeamHasUserDaoInterface {

    @Override
    public Team inviteMemberToTeam(int teamId, List<Users> users) {
        TeamMemberType memberType = get(TeamMemberType.class, TeamMemberTypeConstant.MEMBER_ID);
        TeamMemberStatus teamStatus = get(TeamMemberStatus.class, TeamMemberStatusConstant.INVITE_ID);
        Team team = get(Team.class, teamId);
        for (Users user : users) {
            UserHasTeam inviteUserToTeam = new UserHasTeam();
            inviteUserToTeam.setUsers(user);
            inviteUserToTeam.setTeam(team);
            inviteUserToTeam.setTeamMemberType(memberType);
            inviteUserToTeam.setTeamMemberStatus(teamStatus);
            add(inviteUserToTeam);
        }
        team = (Team) get(Team.class, team.getId());
        return team;
    }

    @Override
    public UserHasTeam updateTeam(UserHasTeam user) {
        user = (UserHasTeam) update(user);
        return user;
    }

    @Override
    public List<UserHasTeam> getUserWhoAcceptInvtation(int userId) {
        List<UserHasTeam> userTeam = new ArrayList<>();
        try {
            Users user = get(Users.class, userId);
            TeamMemberType memberType = get(TeamMemberType.class, TeamMemberTypeConstant.MEMBER_ID);
            TeamMemberStatus teamMemberStatus = get(TeamMemberStatus.class, TeamMemberStatusConstant.ACCEPT_ID);
            List<Team> teamId = new ArrayList<>();
            Set<UserHasTeam> userHasTeam = user.getUserHasTeams();
            for (UserHasTeam userHasTeam1 : userHasTeam) {
                if (userHasTeam1.getTeamMemberType().getId() == TeamMemberTypeConstant.OWNER_ID) {
                    teamId.add(userHasTeam1.getTeam());
                }
            }
            if (teamId.size() > 0) {
                Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
                for (Team team : teamId) {
                    Criteria cr = session.createCriteria(UserHasTeam.class);
                    cr.add(Restrictions.eq("team", team));
                    cr.add(Restrictions.eq("teamMemberStatus", teamMemberStatus));
                    cr.add(Restrictions.eq("teamMemberType", memberType));
                    List<UserHasTeam> teams = (List<UserHasTeam>) cr.list();
                    if (teams != null) {
                        for (UserHasTeam team1 : teams) {
                            userTeam.add(team1);
                        }
                    }
                }
                session.close();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return userTeam;
    }

    @Override
    public UserHasTeam confirm(int userHasTeamId, boolean confirm) {
        TeamMemberStatus teamMemberStatus;
        UserHasTeam userHasTeam = get(UserHasTeam.class, userHasTeamId);
        if (confirm) {
            teamMemberStatus = get(TeamMemberStatus.class, TeamMemberStatusConstant.CONFIRM_ID);
        } else {
            teamMemberStatus = get(TeamMemberStatus.class, TeamMemberStatusConstant.REJECT_ID);
        }
        userHasTeam.setTeamMemberStatus(teamMemberStatus);
        return update(userHasTeam);
    }

    @Override
    public List<Team> getMyTeamAsOwner(int userId) {
        Users user = get(Users.class, userId);
        List<Team> teamId = new ArrayList<>();
        Set<UserHasTeam> userHasTeam = user.getUserHasTeams();
        for (UserHasTeam userHasTeam1 : userHasTeam) {
            if (userHasTeam1.getTeamMemberType().getId() == TeamMemberTypeConstant.OWNER_ID) {
                teamId.add(userHasTeam1.getTeam());
            }
        }
        return teamId;

    }

    @Override
    public List<Team> getMyTeamAsMember(int userId) {
        Users user = get(Users.class, userId);
        List<Team> teamId = new ArrayList<>();
        Set<UserHasTeam> userHasTeam = user.getUserHasTeams();
        for (UserHasTeam userHasTeam1 : userHasTeam) {
            if (userHasTeam1.getTeamMemberType().getId() == TeamMemberTypeConstant.MEMBER_ID
                    && userHasTeam1.getTeamMemberStatus().getId() == TeamMemberStatusConstant.CONFIRM_ID) {
                teamId.add(userHasTeam1.getTeam());
            }
        }
        return teamId;
    }

    @Override
    public List<Users> inviteMoreMemberToTeam(int teamId) {
        List<Users> users = new ArrayList<>();
        try {
            Team team = (Team) get(Team.class, teamId);
            int skillId = 0;
            for (Object object : team.getUserHasTeams()) {
                UserHasTeam userHasTeam = (UserHasTeam) object;
                users.add(userHasTeam.getUsers());
            }
            for (Object object : team.getTeamHasSkills()) {
                TeamHasSkill skill = (TeamHasSkill) object;
                skillId = skill.getSkills().getId();
            }
            Skills skill = get(Skills.class, skillId);
            Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
            List<UserHasSkill> userList = (List<UserHasSkill>) session.createCriteria(UserHasSkill.class).add(Restrictions.not(Restrictions.in("users", users)))
                    .add(Restrictions.eq("skills", skill)).list();
            users.clear();
            session.close();
            for (Object object : userList) {
                UserHasSkill userHasTeam = (UserHasSkill) object;
                users.add(userHasTeam.getUsers());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return users;
    }

    @Override
    public Users getTeamOwner(Team team) {
        TeamMemberType memberType = get(TeamMemberType.class, TeamMemberTypeConstant.OWNER_ID);
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        UserHasTeam userHasSkill = (UserHasTeam) session.createCriteria(UserHasTeam.class)
                .add(Restrictions.eq("team", team)).add(Restrictions.eq("teamMemberType", memberType)).uniqueResult();
        session.close();
        return userHasSkill.getUsers();
    }

    @Override
    public int getTeamHasUserId(Users user, Team team, String type) {
        try {
            TeamMemberType memberType;
            if (type.equals(TeamMemberTypeConstant.OWNER)) {
                memberType = get(TeamMemberType.class, TeamMemberTypeConstant.OWNER_ID);
            } else {
                memberType = get(TeamMemberType.class, TeamMemberTypeConstant.MEMBER_ID);
            }
            Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
            UserHasTeam userHasSkill = (UserHasTeam) session.createCriteria(UserHasTeam.class)
                    .add(Restrictions.eq("users", user)).add(Restrictions.eq("team", team)).add(Restrictions.eq("teamMemberType", memberType)).uniqueResult();
            session.close();
            return userHasSkill.getId();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }
}
