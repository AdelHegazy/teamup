/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao.imp;

import iti36.gp.teamup.constant.TeamMemberStatusConstant;
import iti36.gp.teamup.constant.TeamMemberTypeConstant;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.TeamMemberType;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Hend
 */
//Owner
//
public class MembersOfTeamsHeIsTheOwner extends CommonOperationImp {

    public List<Users> getMembersOfTeam(Team team) {
        List<Users> members = new ArrayList();

        Set userHasTeams = team.getUserHasTeams();
        Iterator iterator = userHasTeams.iterator();
        while (iterator.hasNext()) {

            UserHasTeam obj = (UserHasTeam) iterator.next();
            if (obj.getTeamMemberType().getMemberType()
                    .equals(TeamMemberTypeConstant.MEMBER)
                    //                    
                    && obj.getTeamMemberStatus().getMemberStatus()
                    .equals(TeamMemberStatusConstant.CONFIRM)) {
                members.add(obj.getUsers());
            }

        }

        return members;
    }

    public List<Team> getTeamsOFOwnerX(int id) {

        Users x = get(Users.class, id);
        TeamMemberType getMemebersTybeofOwener = get(TeamMemberType.class, 1);

        Set userHasTeams = x.getUserHasTeams();

        List<Users> owners = new ArrayList<>();

        List<Team> teams = new ArrayList<>();

        Iterator iterator = userHasTeams.iterator();

        //Get All Teams for User X whrere he is the owner
        while (iterator.hasNext()) {
            UserHasTeam iter = (UserHasTeam) iterator.next();
            if (iter.getTeamMemberType().getMemberType()
                    .equals(TeamMemberTypeConstant.OWNER)) {
                teams.add(iter.getTeam());
            }
        }

        return teams;

    }

    //Integrartsed Method 
    public List<Users> getAllMembersOfTeamsForOwnerX(int id) {

        List<Users> allUsersOfTeams = new ArrayList();
//     Set<Users> setofusres=new HashSet(allUsersOfTeams);
        // 1  get Teams 
        List<Team> teamsOFOwnerX = getTeamsOFOwnerX(id);

        // 2 get all members 
        for (Team c : teamsOFOwnerX) {
            allUsersOfTeams.addAll(getMembersOfTeam(c));

        }

        return allUsersOfTeams;

    }

    /*
    public static void main(String args[]) {
        MembersOfTeamsHeIsTheOwner obj = new MembersOfTeamsHeIsTheOwner();
        
        List<Users> allUsersOfTeamsForOwnerX = obj.getAllMembersOfTeamsForOwnerX(69);
        System.out.println("Count : " + allUsersOfTeamsForOwnerX.size());

    

    }*/
}
