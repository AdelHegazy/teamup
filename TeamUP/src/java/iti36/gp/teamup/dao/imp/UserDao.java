/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao.imp;

import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import iti36.gp.teamup.dao.UserDaoInterface;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.UserHasSkill;
import iti36.gp.teamup.util.DbOpenSession;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author adel
 */
public class UserDao extends CommonOperationImp implements UserDaoInterface {


    private static final String HQL_LOGIN = "FROM Users u WHERE u.email =? AND u.password =? ";


    @Override
    public Users signUp(Users user) {
        return (Users) add(user);
    }

    @Override
    public Users updateProfile(Users user) {
        user = (Users) update(user);
        return user;
    }

    @Override
    public Users login(Users user) {
        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.getTransaction().begin();

        Query query = session.createQuery(HQL_LOGIN).setString(0, user.getEmail()).setString(1, user.getPassword());
        List l = query.list();
        if (l.size() > 0) {
            user = (Users) l.get(0);
            System.out.println("logged" + user.getBio());
            session.close();
            return user;
        } else {
            System.out.println("is not logged");
            session.close();
            return null;
        }
    }

    @Override
    public Users getUser(int id) {
        return get(Users.class, id);
    }

    @Override
    public List<UserHasSkill> getUserHasSpecificSkill(int  skillId) {
        Skills skill = get(Skills.class, skillId);
                Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        session.getTransaction().begin();
   Criteria criteria = session.createCriteria(UserHasSkill.class)
                .add(Restrictions.eq("skills", skill));
        return (List<UserHasSkill>)criteria.list();
    }

}
