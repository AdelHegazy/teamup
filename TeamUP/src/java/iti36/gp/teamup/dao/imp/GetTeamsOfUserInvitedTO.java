/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao.imp;

import iti36.gp.teamup.dao.GetInvitationOfTeams;
import iti36.gp.teamup.constant.TeamMemberStatusConstant;
import iti36.gp.teamup.constant.TeamMemberTypeConstant;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.TeamMemberStatus;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.generic.dao.imp.CommonOperationImp;
import iti36.gp.teamup.util.DbOpenSession;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.hibernate.Session;

/**
 *
 * @author Hend
 */
public class GetTeamsOfUserInvitedTO extends CommonOperationImp implements GetInvitationOfTeams {

    @Override
    public List<Team> getInvitedTeamsOfUSer(Users user) {

        Session session = DbOpenSession.getInstance().getSessionfactory().openSession();
        user = (Users) session.get(Users.class, user.getId());
        List<Team> TeamsWithStatusInvite = new ArrayList<>();
        List<TeamMemberStatus> teamMemberStatus = new ArrayList<>();
        for (Iterator<UserHasTeam> iterator = user.getUserHasTeams().iterator(); iterator.hasNext();) {

            UserHasTeam userHasTeam = iterator.next();

            if (userHasTeam.getTeamMemberStatus().getMemberStatus().equals(TeamMemberStatusConstant.INVITE)) {
//              
                TeamsWithStatusInvite.add(userHasTeam.getTeam());
//          invitedTeamsOfUser.add(userHasTeam.getTeam());

            }
        }
//       
        return TeamsWithStatusInvite;

    }
    
    
    
        /*
    public Users getOwnerOfTeam(Team team) {
        Users user = new Users();

        Team team1 = get(Team.class, team.getId());
        Set userHasTeams = team1.getUserHasTeams();
        Iterator iterator = userHasTeams.iterator();
        while (iterator.hasNext()) {
            UserHasTeam obj = (UserHasTeam) iterator.next();
            if (obj.getTeamMemberType().getMemberType()
                    .equals(TeamMemberTypeConstant.OWNER)) {
                user = obj.getUsers();
                break;
            }
        }
        return user;

    }
     */
/*
    public static void main(String args[]) {

        GetTeamsOfUserInvitedTO obj = new GetTeamsOfUserInvitedTO();

        Set<Team> confirmedTeamsOfUSer = obj.getConfirmedTeamsOfUSer(5);
        Set<Users> ownersOfTeam = obj.getOwnersOfTeams(confirmedTeamsOfUSer);
        for (Users tempuser : ownersOfTeam) {
            System.out.println("Owners : " + tempuser.getId());
        }
*/
    

}
//      

