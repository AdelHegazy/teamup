/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao;

import iti36.gp.teamup.entity.SkillCategory;
import java.util.List;

/**
 *
 * @author adel
 */
public interface CategoryDaoInterface {

    public List<SkillCategory> getAllCategory();
}
