/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.dao;

import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.TeamHasSkill;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import java.util.List;

/**
 *
 * @author AhmedElGazzar
 */
public interface AdminDaoInterface {
 
    public Users adminLogin(String email, String passwd);
    
    public List<Skills> getWaitingSkills();
    
    public List<Skills> getCountOfWaitingSkills();
    
    public List<Skills> getAllSkills();
    
    public void acvtiveSkills(int skillsId);
    
    public void deacvtiveSkills(int skillsId);   
    
    public void addSkill(int categoryId, String skill);
        
    public void addCategory(String cateName);
    
    public List<SkillCategory> getAllSkillCategory();
    
    public List<UserHasTeam> getTeamReport();
        
    public List<UserHasTeam> getTeamMembersByUser(int teamId);
    
    public List<TeamHasSkill> getTeamSkills(int teamId);
    
    public boolean validateSkill(String name);
    
    public boolean validateSkillCategory(String name);
}
