/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.service;

import iti36.gp.teamup.entity.MessageHasUsers;
import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import java.util.List;

/**
 *
 * @author adel
 */
public interface CommonServiceLayer {

    public Users login(Users user);

    public Users logOut(int userId);

    public Users registration(Users user);

    public Users updateProfile(Users user);

    public Team createTeam(Team team, int owner, int skill);

    public List<Users> getSuggestionUsersForNewTeam(int skillId, int owner);

    public Team inviteMembersToTeam(int team, List<Users> users);

    public Users operationOnInvitation(int teamId, int userId, boolean accept);

    public boolean confirmInviteToTeam(int teamId, int userId);

    public List<Users> getAllUsers();

    public List<SkillCategory> getAllCategory();

    public Users addSkill(int userId, List<Integer> skill, int levelId);

    public List<Team> getInvitedTeamsOfUSerService(Users user);

    public List<UserHasTeam> getUserWhoAcceptInvtation(int userId);

    public UserHasTeam confirm(int userHasTeamId, boolean confirm);

    public Users addNewSkill(int userId, int categoryId, String skillName);

    public Users getUser(int userId);

    public Team getTeam(int userId);

    public Team changeTeamStatus(Team team, boolean cancel);

    public List<String> getAllMessagesBetween2Users(int userLoggedIn, int UserSelected);

    public List<MessageHasUsers> getListOfUsersHasMessage(int to, int from);

    public boolean sendMessage(String message, int from, int to);

    public List<Users> getAllUsersForChat(int id);

    public List<Team> getMyTeamAsOwner(int userId);

    public List<Team> getMyTeamAsMember(int userId);

    public Users removeUserSkill(int userId, int skillId);

    public List<Users> inviteMoreMemberToTeam(int teamId);
    
    public Users getTeamOwner(Team team);
    
     public int getTeamHasUserId(Users user, Team team, String type); 

}
