/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.service.dao.imp;

import iti36.gp.teamup.dao.imp.MembersOfTeamsHeIsTheOwner;
import iti36.gp.teamup.dao.imp.OwnersofTeamsHeIsMemberOn;
import iti36.gp.teamup.dao.imp.Senders;
import iti36.gp.teamup.constant.TeamMemberStatusConstant;
import iti36.gp.teamup.dao.imp.CategoryDao;
import iti36.gp.teamup.dao.imp.GetTeamsOfUserInvitedTO;
import iti36.gp.teamup.dao.imp.SendAndGetMessagesHistory;
import iti36.gp.teamup.dao.imp.SkillsDao;
import iti36.gp.teamup.dao.imp.TeamDao;
import iti36.gp.teamup.dao.imp.TeamHasUserDao;
import iti36.gp.teamup.dao.imp.UserDao;
import iti36.gp.teamup.entity.MessageHasUsers;
import iti36.gp.teamup.entity.SkillCategory;
import iti36.gp.teamup.entity.Skills;
import iti36.gp.teamup.entity.Team;
import iti36.gp.teamup.entity.TeamHasSkill;
import iti36.gp.teamup.entity.UserHasSkill;
import iti36.gp.teamup.entity.UserHasTeam;
import iti36.gp.teamup.entity.Users;
import iti36.gp.teamup.service.CommonServiceLayer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author adel
 */
public class RegistrationService implements CommonServiceLayer {

    @Override
    public Users login(Users user) {
        UserDao userDao = new UserDao();
        return userDao.login(user);
    }

    @Override
    public Users logOut(int userId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Users registration(Users user) {
        UserDao userDao = new UserDao();
        return userDao.signUp(user);
    }

    @Override
    public Users updateProfile(Users user) {

        UserDao userDao = new UserDao();
        return userDao.updateProfile(user);

    }

    @Override
    public Team createTeam(Team team, int owner, int skill) {
        Skills skillId = new Skills();
        skillId.setId(skill);
        TeamHasSkill teamHasSkill = new TeamHasSkill();
        teamHasSkill.setSkills(skillId);
        teamHasSkill.setTeam(team);
        TeamDao teamDo = new TeamDao();
        return teamDo.createTeam(teamHasSkill, owner);
    }

    @Override
    public List<Users> getSuggestionUsersForNewTeam(int skillId, int owner) {

        UserDao userDao = new UserDao();
        List<UserHasSkill> usersHasSkills = userDao.getUserHasSpecificSkill(skillId);
        List<Users> suggestionUsers = new ArrayList<>();
        for (int i = 0; i < usersHasSkills.size(); i++) {
            if (usersHasSkills.get(i).getUsers().getId().equals(owner)) {
                usersHasSkills.remove(i);
                i--;
            } else {
                suggestionUsers.add(usersHasSkills.get(i).getUsers());
            }
        }
        return suggestionUsers;
    }

    @Override
    public Team inviteMembersToTeam(int teamId, List<Users> users) {
        TeamHasUserDao invite = new TeamHasUserDao();
        return invite.inviteMemberToTeam(teamId, users);
    }

    @Override
    public boolean confirmInviteToTeam(int teamId, int userId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Users> getAllUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll(Users.class);
    }

    @Override
    public List<SkillCategory> getAllCategory() {
        CategoryDao category = new CategoryDao();
        return category.getAllCategory();
    }

    @Override
    public Users addSkill(int userId, List<Integer> skill, int levelId) {
        SkillsDao category = new SkillsDao();
        Users user = category.addSkillToUser(userId, skill, levelId);
        return user;
    }

    @Override
    public List<Team> getInvitedTeamsOfUSerService(Users user) {
        GetTeamsOfUserInvitedTO teams = new GetTeamsOfUserInvitedTO();
        return teams.getInvitedTeamsOfUSer(user);
    }

    @Override
    public Users operationOnInvitation(int teamId, int userId, boolean accept) {
        UserDao userDao = new UserDao();
        TeamHasUserDao teamDao = new TeamHasUserDao();
        Users user = userDao.getUser(userId);
        for (Object object : user.getUserHasTeams()) {
            UserHasTeam userHasTeam = (UserHasTeam) object;
            if (userHasTeam.getTeam().getId().equals(teamId)) {
                if (accept) {
                    userHasTeam.getTeamMemberStatus().setMemberStatus((TeamMemberStatusConstant.ACCEPT));
                    userHasTeam.getTeamMemberStatus().setId(TeamMemberStatusConstant.ACCEPT_ID);
                } else {
                    userHasTeam.getTeamMemberStatus().setMemberStatus((TeamMemberStatusConstant.REJECT));
                    userHasTeam.getTeamMemberStatus().setId(TeamMemberStatusConstant.REJECT_ID);
                }
                teamDao.updateTeam(userHasTeam);
                break;
            }
        }
        return user;
    }

    @Override
    public List<UserHasTeam> getUserWhoAcceptInvtation(int userId) {
        TeamHasUserDao teamDao = new TeamHasUserDao();
        List<UserHasTeam> users = teamDao.getUserWhoAcceptInvtation(userId);
        return users;
    }

    @Override
    public UserHasTeam confirm(int userHasTeamId, boolean confirm) {
        TeamHasUserDao teamDao = new TeamHasUserDao();
        return teamDao.confirm(userHasTeamId, confirm);
    }

    @Override
    public Users addNewSkill(int userId, int categoryId, String skillName) {
        SkillsDao skilDao = new SkillsDao();
        return skilDao.addNewSkill(userId, categoryId, skillName);
    }

    @Override
    public Users getUser(int userId) {
        UserDao userDao = new UserDao();
        return userDao.getUser(userId);
    }

    @Override
    public Team getTeam(int teamId) {
        TeamDao teamDao = new TeamDao();
        return teamDao.getTeamById(teamId);
    }

    @Override
    public Team changeTeamStatus(Team team, boolean cancel) {
        TeamDao teamDao = new TeamDao();
        return teamDao.changeTeamStatus(team, cancel);
    }

    @Override
    public List<String> getAllMessagesBetween2Users(int userLoggedIn, int UserSelected) {
        SendAndGetMessagesHistory obj = new SendAndGetMessagesHistory();
        return obj.getAllMessagesBetween2Users(userLoggedIn, UserSelected);
    }

    @Override
    public List<MessageHasUsers> getListOfUsersHasMessage(int to, int from) {
        SendAndGetMessagesHistory obj = new SendAndGetMessagesHistory();
        return obj.getListOfUsersHasMessage(to, from);
    }

    @Override
    public boolean sendMessage(String message, int from, int to) {
        SendAndGetMessagesHistory obj = new SendAndGetMessagesHistory();
        return obj.sendMessage(message, from, to);
    }

    @Override
    public List<Users> getAllUsersForChat(int id) {
        MembersOfTeamsHeIsTheOwner members = new MembersOfTeamsHeIsTheOwner();
        OwnersofTeamsHeIsMemberOn owners = new OwnersofTeamsHeIsMemberOn();
        Senders senders = new Senders();
        List<Users> allUsers = new ArrayList();
        List<Users> allUsersOfTeamsForOwnerX = members.getAllMembersOfTeamsForOwnerX(id);
        List<Users> senderForUser = senders.getSenderForUser(id);
        List<Users> ownersOfTeams = owners.getOwnersOfTeams(id);
        allUsers.addAll(allUsersOfTeamsForOwnerX);
        allUsers.addAll(senderForUser);
        allUsers.addAll(ownersOfTeams);
        for (Users tmp : allUsers) {
            System.out.println("Name : " + tmp.getEmail());
        }
        Set<Users> set = new HashSet(allUsers);
        for (int i = 0; i < allUsers.size(); i++) {
            Users item = allUsers.get(i);
            int id1 = item.getId();
            for (int j = i + 1; j < allUsers.size(); j++) {
                Users item2 = allUsers.get(j);
                if (item2.getId() == id1) {

                    allUsers.remove(item2);
                    j--;
                }

            }

        }

        return allUsers;
    }

    @Override
    public List<Team> getMyTeamAsOwner(int userId) {
        TeamHasUserDao teamHasUserDao = new TeamHasUserDao();
        return teamHasUserDao.getMyTeamAsOwner(userId);
    }

    @Override
    public List<Team> getMyTeamAsMember(int userId) {
        TeamHasUserDao teamHasUserDao = new TeamHasUserDao();
        return teamHasUserDao.getMyTeamAsMember(userId);
    }

    @Override
    public Users removeUserSkill(int userId, int skillId) {
        SkillsDao skill = new SkillsDao();
        return skill.removeUserSkill(userId, skillId);
    }

    @Override
    public List<Users> inviteMoreMemberToTeam(int teamId) {
        TeamHasUserDao teamDao = new TeamHasUserDao();
        return teamDao.inviteMoreMemberToTeam(teamId);
    }

    @Override
    public Users getTeamOwner(Team team) {
        TeamHasUserDao teamDao = new TeamHasUserDao();
        return teamDao.getTeamOwner(team);
    }

    @Override
    public int getTeamHasUserId(Users user, Team team, String type) {
            TeamHasUserDao teamDao = new TeamHasUserDao();
            return teamDao.getTeamHasUserId(user, team, type);
    }

}
