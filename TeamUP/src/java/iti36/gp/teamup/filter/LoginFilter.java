/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gp.teamup.filter;

import iti36.gp.teamup.beans.LoginBean;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author adel
 */
public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            HttpServletRequest reqt = (HttpServletRequest) request;
            HttpServletResponse resp = (HttpServletResponse) response;
            LoginBean session = (LoginBean) reqt.getSession().getAttribute("loginBean");
            String reqURI = reqt.getRequestURI();
            System.out.println("iti36.gp.teamup.filter.LoginFilter.doFilter()" + reqURI);
            if ((session == null || !session.isLogged)) {
                if (reqURI.endsWith("/TeamUP/") || reqURI.indexOf("/signup.xhtml") >= 0
                 || reqURI.indexOf("/index.xhtml") >= 0 || reqURI.indexOf("javax.faces.resource") >= 0
                 || reqURI.indexOf("fonts") >= 0 || reqURI.indexOf("images")>=0 || reqURI.indexOf("rest")>=0) {
                    chain.doFilter(request, response);
                } else {
                    resp.sendRedirect(reqt.getServletContext().getContextPath() + "/index.xhtml");
                }
            } else {
                chain.doFilter(request, response);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    @Override
    public void destroy() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
