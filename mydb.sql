-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 11, 2016 at 03:32 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `message_content`) VALUES
(1, 'hi'),
(2, 'hi'),
(3, 'hi'),
(4, 'hi'),
(5, 'hi'),
(6, 'hi'),
(7, 'hi');

-- --------------------------------------------------------

--
-- Table structure for table `message_has_users`
--

CREATE TABLE IF NOT EXISTS `message_has_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL,
  `email_to` int(11) NOT NULL,
  `email_from` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_has_users_users1_idx` (`email_to`),
  KEY `fk_message_has_users_message1_idx` (`message_id`),
  KEY `fk_message_has_users_users2_idx1` (`email_from`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `message_has_users`
--

INSERT INTO `message_has_users` (`id`, `message_id`, `email_to`, `email_from`) VALUES
(1, 1, 1, 1),
(2, 7, 38, 39);

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `category_id` int(11) NOT NULL,
  `skills_status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_skill_category1_idx` (`category_id`),
  KEY `fk_skill_skills_status1_idx` (`skills_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `skills_status`
--

CREATE TABLE IF NOT EXISTS `skills_status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `skill_category`
--

CREATE TABLE IF NOT EXISTS `skill_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` text,
  `image` longblob,
  `start_date` timestamp NULL DEFAULT NULL,
  `duration_days` int(11) DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `short_description` varchar(250) NOT NULL,
  `team_status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_team_team_status1_idx` (`team_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `title`, `description`, `image`, `start_date`, `duration_days`, `end_date`, `short_description`, `team_status_id`) VALUES
(4, 'ss', 's', NULL, '2016-05-10 16:01:29', 3, '2016-05-10 16:01:29', '333', 1);

-- --------------------------------------------------------

--
-- Table structure for table `team_has_skill`
--

CREATE TABLE IF NOT EXISTS `team_has_skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_team_has_skill_skill1_idx` (`skill_id`),
  KEY `fk_team_has_skill_team1_idx` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `team_member_status`
--

CREATE TABLE IF NOT EXISTS `team_member_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `team_member_status`
--

INSERT INTO `team_member_status` (`id`, `member_status`) VALUES
(1, 'accept'),
(2, 'reject'),
(3, 'confirm'),
(4, 'invite');

-- --------------------------------------------------------

--
-- Table structure for table `team_member_type`
--

CREATE TABLE IF NOT EXISTS `team_member_type` (
  `id` int(11) NOT NULL,
  `member_type` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team_member_type`
--

INSERT INTO `team_member_type` (`id`, `member_type`) VALUES
(1, 'owner'),
(2, 'member');

-- --------------------------------------------------------

--
-- Table structure for table `team_status`
--

CREATE TABLE IF NOT EXISTS `team_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `team_status`
--

INSERT INTO `team_status` (`id`, `status`) VALUES
(1, 'cancelled'),
(2, 'active'),
(3, 'expired');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `bio` text,
  `url` text,
  `profile_picture` longblob,
  `token` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `bio`, `url`, `profile_picture`, `token`) VALUES
(1, 'Adel', 'adel@yahoo.com', '!23', NULL, NULL, NULL, NULL, NULL),
(2, 'adel0', 'adel@gmail.com0', '12345670', NULL, NULL, NULL, NULL, NULL),
(3, 'adel1', 'adel@gmail.com1', '12345671', NULL, NULL, NULL, NULL, NULL),
(4, 'adel2', 'adel@gmail.com2', '12345672', NULL, NULL, NULL, NULL, NULL),
(5, 'adel3', 'adel@gmail.com3', '12345673', NULL, NULL, NULL, NULL, NULL),
(6, 'adel4', 'adel@gmail.com4', '12345674', NULL, NULL, NULL, NULL, NULL),
(7, 'adel5', 'adel@gmail.com5', '12345675', NULL, NULL, NULL, NULL, NULL),
(8, 'adel6', 'adel@gmail.com6', '12345676', NULL, NULL, NULL, NULL, NULL),
(9, 'adel7', 'adel@gmail.com7', '12345677', NULL, NULL, NULL, NULL, NULL),
(10, 'adel8', 'adel@gmail.com8', '12345678', NULL, NULL, NULL, NULL, NULL),
(11, 'adel9', 'adel@gmail.com9', '11111111', NULL, NULL, NULL, NULL, NULL),
(12, 'hima', 'ss', '1234', NULL, NULL, NULL, NULL, NULL),
(13, 'adel10', 'adel@gmail.com10', '123456710', NULL, NULL, NULL, NULL, NULL),
(14, 'adel11', 'adel@gmail.com11', '123456711', NULL, NULL, NULL, NULL, NULL),
(15, 'adel12', 'adel@gmail.com12', '123456712', NULL, NULL, NULL, NULL, NULL),
(16, 'adel13', 'adel@gmail.com13', '123456713', NULL, NULL, NULL, NULL, NULL),
(17, 'adel14', 'adel@gmail.com14', '123456714', NULL, NULL, NULL, NULL, NULL),
(19, 'hima3', 'ss3', '12343', NULL, NULL, NULL, NULL, NULL),
(20, 'hima3sss', '0ss3', '12343', NULL, NULL, NULL, NULL, NULL),
(21, 'himass3e', 'osseeee3', '12343', NULL, NULL, NULL, NULL, NULL),
(23, 'himaee2343sss', '02', '12343', NULL, NULL, NULL, NULL, NULL),
(24, 'him34e', 'o', '12343', NULL, NULL, NULL, NULL, NULL),
(26, 'himaee2343szss', '02z', '12343', NULL, NULL, NULL, NULL, NULL),
(27, 'him34e', 'ozz', '12343', NULL, NULL, NULL, NULL, NULL),
(28, 'himaee2343s4zss', '042z', '12343', NULL, NULL, NULL, NULL, NULL),
(29, 'hi4m34e', 'o4zz', '12343', NULL, NULL, NULL, NULL, NULL),
(31, 'himaee2343sz4ss', '024z', '12343', NULL, NULL, NULL, NULL, NULL),
(32, 'him344e', 'oz4z', '12343', NULL, NULL, NULL, NULL, NULL),
(33, 'himaeee2343sz4ss', '0e24z', '12343', NULL, NULL, NULL, NULL, NULL),
(34, 'him344ee', 'eoz4z', '12343', NULL, NULL, NULL, NULL, NULL),
(36, 'himaeee2343sdz4ss', '0ed24z', '12343', NULL, NULL, NULL, NULL, NULL),
(37, 'him344dee', 'deoz4z', '12343', NULL, NULL, NULL, NULL, NULL),
(38, 's', 's', '12343', NULL, NULL, NULL, NULL, NULL),
(39, 'a', 'd', '12343', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_has_skill`
--

CREATE TABLE IF NOT EXISTS `user_has_skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `skill_skill_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_has_skill_skill1_idx` (`skill_skill_id`),
  KEY `fk_user_has_skill_user1_idx` (`user_id`),
  KEY `fk_user_has_skill_level1_idx` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_has_team`
--

CREATE TABLE IF NOT EXISTS `user_has_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `team_member_status_id` int(11) NOT NULL,
  `team_member_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_User_has_Team_Team1_idx` (`team_id`),
  KEY `fk_User_has_Team_User_idx` (`user_id`),
  KEY `fk_user_has_team_team_member_status1_idx` (`team_member_status_id`),
  KEY `fk_user_has_team_team_member_type1_idx` (`team_member_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=147 ;

--
-- Dumping data for table `user_has_team`
--

INSERT INTO `user_has_team` (`id`, `user_id`, `team_id`, `team_member_status_id`, `team_member_type_id`) VALUES
(85, 1, 4, 4, 2),
(86, 2, 4, 4, 2),
(87, 3, 4, 4, 2),
(88, 4, 4, 4, 2),
(89, 5, 4, 4, 2),
(90, 6, 4, 4, 2),
(91, 7, 4, 4, 2),
(92, 8, 4, 4, 2),
(93, 9, 4, 4, 2),
(94, 10, 4, 4, 2),
(95, 11, 4, 4, 2),
(96, 12, 4, 4, 2),
(97, 13, 4, 4, 2),
(98, 14, 4, 4, 2),
(99, 15, 4, 4, 2),
(100, 16, 4, 4, 2),
(101, 17, 4, 4, 2),
(102, 19, 4, 4, 2),
(103, 20, 4, 4, 2),
(104, 21, 4, 4, 2),
(105, 23, 4, 4, 2),
(106, 24, 4, 4, 2),
(107, 26, 4, 4, 2),
(108, 27, 4, 4, 2),
(109, 28, 4, 4, 2),
(110, 29, 4, 4, 2),
(111, 31, 4, 4, 2),
(112, 32, 4, 4, 2),
(113, 1, 4, 4, 2),
(114, 2, 4, 4, 2),
(115, 3, 4, 4, 2),
(116, 4, 4, 4, 2),
(117, 5, 4, 4, 2),
(118, 6, 4, 4, 2),
(119, 7, 4, 4, 2),
(120, 8, 4, 4, 2),
(121, 9, 4, 4, 2),
(122, 10, 4, 4, 2),
(123, 11, 4, 4, 2),
(124, 12, 4, 4, 2),
(125, 13, 4, 4, 2),
(126, 14, 4, 4, 2),
(127, 15, 4, 4, 2),
(128, 16, 4, 4, 2),
(129, 17, 4, 4, 2),
(130, 19, 4, 4, 2),
(131, 20, 4, 4, 2),
(132, 21, 4, 4, 2),
(133, 23, 4, 4, 2),
(134, 24, 4, 4, 2),
(135, 26, 4, 4, 2),
(136, 27, 4, 4, 2),
(137, 28, 4, 4, 2),
(138, 29, 4, 4, 2),
(139, 31, 4, 4, 2),
(140, 32, 4, 4, 2),
(141, 33, 4, 4, 2),
(142, 34, 4, 4, 2),
(143, 36, 4, 4, 2),
(144, 37, 4, 4, 2),
(145, 38, 4, 4, 2),
(146, 39, 4, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_skill_level`
--

CREATE TABLE IF NOT EXISTS `user_skill_level` (
  `id` int(11) NOT NULL,
  `level` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `message_has_users`
--
ALTER TABLE `message_has_users`
  ADD CONSTRAINT `fk_message_has_users_message1` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_message_has_users_users1` FOREIGN KEY (`email_to`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_message_has_users_users2` FOREIGN KEY (`email_from`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `skills`
--
ALTER TABLE `skills`
  ADD CONSTRAINT `fk_skill_category1` FOREIGN KEY (`category_id`) REFERENCES `skill_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_skill_skills_status1` FOREIGN KEY (`skills_status_id`) REFERENCES `skills_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `fk_team_team_status1` FOREIGN KEY (`team_status_id`) REFERENCES `team_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `team_has_skill`
--
ALTER TABLE `team_has_skill`
  ADD CONSTRAINT `fk_team_has_skill_skill1` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_team_has_skill_team1` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_has_skill`
--
ALTER TABLE `user_has_skill`
  ADD CONSTRAINT `fk_user_has_skill_level1` FOREIGN KEY (`level_id`) REFERENCES `user_skill_level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_skill_skill1` FOREIGN KEY (`skill_skill_id`) REFERENCES `skills` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_skill_user1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_has_team`
--
ALTER TABLE `user_has_team`
  ADD CONSTRAINT `fk_User_has_Team_Team1` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_team_team_member_status1` FOREIGN KEY (`team_member_status_id`) REFERENCES `team_member_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_team_team_member_type1` FOREIGN KEY (`team_member_type_id`) REFERENCES `team_member_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_User_has_Team_User` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
